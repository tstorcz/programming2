```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printInts(int val1, int val2) {
    val1 = 82;
    printf("%d <-> %d\n", val1, val2);
}

void printStrings(char text1[], char text2[]) {
    text1[2] = 'O';
    printf("%s\n%s\n", text1, text2);
}

void printIntArray(int vals[]) {
    vals[0] = 28;
    printf("%d <-> %d\n", vals[0], vals[1]);
}

void printIntPointers(int *val1, int *val2) {
    *val1 = 82;
    printf("%d <-> %d\n", *val1, *val2);
}

void copyTextArray(char dest[], char src[])
{
    //Also copy closing \0
    for(int i = 0; i <= strlen(src); i++) {
        dest[i] = src[i];
    }
}

char *cloneTextArray (char source[])
{
    int length = strlen(source);
    //Size is length+1 to store closing \0
    char dest[length + 1];
    
    //Also copy closing \0
    for(int i = 0; i <= length; i++)
        dest[i] = source[i];
        
    return dest;
}

char *cloneText (char *source)
{
    //Size is length+1 to store closing \0
    char *dest = (char *)calloc(strlen(source) + 1, sizeof(char));
    char *d = dest;
    
    //Will this copy closing \0?
    while(*d++ = *source++);
    
    return dest;
}

int main()
{
    //Variables
    int val1 = 2;
    int val2 = 4;
    double dval = 7.89;
    printf("%d, %d, %f\n", val1, val2, dval);
    
    //Difference of types
    //Value representation space and format -> range, precision
    printf("Size of char %ld, int: %ld, double: %ld\n", sizeof(char), sizeof(int), sizeof(double));
    
    //Code block: sequence of control statements and instructions
    int i = 99;
    printf("i: %d\n", i);
    for(int i=0;i<10;i++)
        printf("i: %d\n", i);
    printf("i: %d\n", i);
    
    //Print int variables
    printf("%d <-> %d\n", val1, val2);
    
    //Methods: formal and actual parameters
    //Change values
    printInts(val1, val2);
    
    //Print int variables. Have they changed?
    printf("%d <-> %d\n", val1, val2);
    
    //Passing double as int?
    //Type conversion: cast
    printInts(dval, dval);
    
    //Arrays
    //String is a char array terminated by \0
    char text1[] = "Hello world";
    char text2[] = "This is C here";
    
    //Modifying in method. Have they changed?
    printf("%s\n%s\n", text1, text2);
    printStrings(text1, text2);
    printf("%s\n%s\n", text1, text2);
    
    //Create array from ints. Hae they changed?
    printIntArray((int[]){val1, val2});
    printf("%d <-> %d\n", val1, val2);
    
    //Use pointers. Have int variables changed?
    printIntPointers(&val1, &val2);
    printf("%d <-> %d\n", val1, val2);
    
    
    char copyOfText[15];
    char nextText[30] = "Houston, we have a problem...";

    printf ("original: %s; %s\n", text2, nextText);
    copyTextArray(copyOfText, "Copy this text");
    printf ("copy: %s, %s\n", copyOfText, nextText);
    
    //How text length is handled?
    copyTextArray(copyOfText, "Copy this very long text");
    printf ("copy: %s; %s\n", copyOfText, nextText);

    //How to create new char array?
    printf("\n\n");
    printf("original: %s\n", text2);
    printf("clone 1: %s\n", cloneTextArray(text2));
    
    char *cloneofText = cloneText(text2);
    printf("clone 2: %s\n", cloneofText);
    free (cloneofText);
}

```