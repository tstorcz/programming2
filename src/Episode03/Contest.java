package Episode03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Contest {
    private final String name;
    private final String dataPath;
    private Participant[] participants;
    private Jury[] juries;

    public String getName() {
        return name;
    }

    public Contest(String name, String dataPath) {
        this.name = name;
        this.dataPath = dataPath;
    }

    public void printParticipants() {
        for (Participant participant: participants) {
            System.out.format(
                    "name: %s (#%d) points: %d\n",
                    participant.getName(),
                    participant.getId(),
                    participant.getPoints()
            );
        }
    }

    public void printJury() {
        for(Jury jury: juries) {
            System.out.format(
              "Name: %s\n",
              jury.getName()
            );
        }
    }

    public void sortParticipants() {
        boolean hasChange;
        do {
            hasChange = false;
            for (int i = 0; i < participants.length - 1; i++) {
                if (participants[i].getPoints() < participants[i + 1].getPoints()) {
                    Participant temp;
                    temp = participants[i];
                    participants[i] = participants[i + 1];
                    participants[i + 1] = temp;
                    hasChange = true;
                }
            }
        }while(hasChange);
    }

    public void start(
            Scanner scanner
    ) throws FileNotFoundException {
        recruitJury(scanner);
        recruitJuryFromFile(dataPath + "\\Ep03\\juries.csv");

        recruitParticipants(scanner);
        recruitParticipantsFromFile(dataPath + "\\Ep03\\participants.csv");

        printJury();
        System.out.println("Initial ranking");
        printParticipants();

        evaluateParticipants();

        sortParticipants();
        System.out.println("Final result");
        printParticipants();
    }

    private void recruitJury(Scanner scanner) {
        System.out.print("Enter number of jury: ");
        int numberOfJuries = scanner.nextInt();
        scanner.nextLine();

        juries = new Jury[numberOfJuries];
        for (int i = 0; i < numberOfJuries; i++) {
            System.out.format("Enter %d. jury name: ", i+1);
            String name = scanner.nextLine();

            System.out.format("Enter %d. jury age: ", i+1);
            int age = scanner.nextInt();
            scanner.nextLine();

            juries[i] = new Jury(name, age);
        }
    }

    private void recruitJuryFromFile(String fileName) throws FileNotFoundException {
        File source = new File(fileName);
        Scanner fileReader = new Scanner(source);
        int lineCount = 0;
        while(fileReader.hasNextLine()) {
            fileReader.nextLine();
            lineCount++;
        }
        fileReader.close();

        String[] values;
        if(lineCount > 0) {
            source = new File(fileName);
            fileReader = new Scanner(source);
            juries = new Jury[lineCount];
            for (int i = 0; i < lineCount; i++) {
                values = fileReader.nextLine().split(";");
                juries[i] = new Jury(
                        values[0],
                        Integer.parseInt(values[1])
                );
            }
        }
    }

    private void recruitParticipants(Scanner scanner) {
        System.out.print("Enter number of participants: ");
        int numberOfParticipants = scanner.nextInt();
        scanner.nextLine();

        participants = new Participant[numberOfParticipants];

        ParticipantFactory pf = new ParticipantFactory(new IdGenerator());

        for (int i = 0; i < participants.length; i++) {
            System.out.format("Enter %d. participant name: ", i+1);
            String name = scanner.nextLine();

            System.out.format("Enter %d. participant age: ", i+1);
            int age = scanner.nextInt();
            scanner.nextLine();

            participants[i] = ParticipantFactory.createParticipant(name, age);
            participants[i] = pf.createParticipant1(name, age);
        }
    }

    private void recruitParticipantsFromFile(String fileName) throws FileNotFoundException {
        File source = new File(fileName);
        Scanner fileReader = new Scanner(source);
        int lineCount = 0;
        while(fileReader.hasNextLine()) {
            fileReader.nextLine();
            lineCount++;
        }
        fileReader.close();

        String[] values;
        if(lineCount > 0) {
            source = new File(fileName);
            fileReader = new Scanner(source);
            participants = new Participant[lineCount];
            for (int i = 0; i < lineCount; i++) {
                values = fileReader.nextLine().split(";");
                participants[i] = ParticipantFactory.createParticipant(values[0],
                        Integer.parseInt(values[1])
                );
            }
        }
    }

    private void evaluateParticipants() {
        int points;
        for(Participant participant: participants) {
            System.out.format("Evaluating %s\n", participant.getName());
            for(Jury jury: juries) {
                points = jury.givePoint();

                System.out.format(
                        "\tPoints from %s: %d\n",
                        jury.getName(),
                        points
                );

                participant.addPoints(points);
            }
        }
    }
}
