package Episode03;

public class IdGenerator {
    //ID generator could implement more complex ID creation algorithm

    private int lastParticipantId = 0;

    public int getNextId() {
        return ++lastParticipantId;
    }
}
