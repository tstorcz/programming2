package Episode03;

public class ParticipantFactory {
    //private static int numberOfParticipantsCreated = 0;

    //ParticipantFactory should be responsible only for participant creation,
    // not for ID management
    private static IdGenerator classGenerator = new IdGenerator();

    public static Participant createParticipant(
            String name,
            int age
    ) {
        //Using ID generator stored in class property
        //return new Participant(name, age, ++numberOfParticipantsCreated);
        return new Participant(name, age, classGenerator.getNextId());
    }

    //Solve factory task in instances
    // handling ID generator as instance parameter could add flexibility
    private IdGenerator instanceGenerator;
    public ParticipantFactory(IdGenerator idGenerator) {
        instanceGenerator = idGenerator;
    }

    public Participant createParticipant1(
            String name,
            int age
    ) {
        //Using ID generator set in instance constructor
        return new Participant(name, age, instanceGenerator.getNextId());
    }

    public Participant createParticipant2(
            String name,
            int age
    ) {
        //Using ID generator stored in class property
        return new Participant(name, age, classGenerator.getNextId());
    }
}
