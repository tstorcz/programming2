package Episode03;

import java.util.Random;

public class Jury {
    private static int maxPoint = 10;

    public static void setMaxPoint(int maxPoint) {
        Jury.maxPoint = maxPoint;
    }

    private static Random rnd = new Random();

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Jury(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int givePoint() {
        return rnd.nextInt(maxPoint) + 1;
    }
}
