package Episode03;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public void main(int number, String dataPath) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter title of show: ");
        Contest contest = new Contest(scanner.nextLine(), dataPath);

        System.out.print("Maximum of points: ");
        Jury.setMaxPoint(scanner.nextInt());
        scanner.nextLine();

        contest.start(scanner);
    }
}
