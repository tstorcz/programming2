package Episode01;

import java.util.Scanner;

public class BasicAlgorithms {
    public static void Print() {
        int[] t = new int[] {5, 8, 3, 7, 6, 9, 2, 4};

        // Try enhanced for
        for(int i = 0; i < t.length; i++){
            System.out.println(t[i]);
        }
    }

    public static void Sum() {
        int[] t = new int[] {5, 8, 3, 7, 6, 9, 2, 4};

        int sum = 0;
        // Try enhanced for
        for(int i = 0; i < t.length; i++){
            sum += t[i];
        }

        // Insert data into printed text message: %d - insert decimal parameter
        // %n results Enter (CR+LF)
        System.out.printf("Sum: %d%n", sum);
    }

    public static void Extrema() {
        int[] t = new int[] {5, 8, 3, 7, 6, 9, 2, 4};

        int min = t[0];
        int max = t[0];

        for(int i = 1; i < t.length; i++){
            if(min > t[i])min = t[i];
            if(max < t[i])max = t[i];
        }

        System.out.print("Min: "); System.out.println(min);
        System.out.print("Max: "); System.out.println(max);
    }

    public static void Selection() {
        int[] t = new int[] {5, 8, 3, 7, 6, 9, 2, 4};
        int[] selected = new int[t.length];

        int found = 0;
        for(int i = 0; i < t.length; i++){
            if(t[i] >= 5) {
                selected[found++] = t[i];
            }
        }

        System.out.print("Items higher or equal to 5: ");
        for(int i = 0; i < found; i++){
            System.out.print(selected[i]);
            System.out.print(", ");
        }
        System.out.println();
    }

    public static void Sort() {
        int[] t = new int[] {5, 8, 3, 7, 6, 9, 2, 4};

        PrintArray(t);

        for(int i=0; i < t.length; i++)
            for(int j=0; j < t.length - 1; j++)
                if(t[j] > t[j+1]) {
                    int tmp = t[j];
                    t[j] = t[j+1];
                    t[j+1] = tmp;
                }

        PrintArray(t);
    }

    public static void SumManualInput() {
        System.out.print("Number count: ");

        //To read data from console, a Scanner is required
        //the Scanner scans the default input of the system (which is the console)
        Scanner input = new Scanner(System.in);
        //Read a string from the console using Scanner
        String enteredLine = input.next();

        //Convert string to integer number
        int numberCount = Integer.parseInt(enteredLine);

        int sum = 0;
        for(int i = 1; i <= numberCount; i++){
            System.out.print("Number ");
            System.out.print(i);
            System.out.print(": ");

            //Do things in one step: get string, convert to int, calculate sum
            sum += Integer.parseInt(input.next());
        }

        System.out.print("Sum: ");
        System.out.println(sum);
    }
    
    public static void PrintArray(int[] arr) {
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
            System.out.print(", ");
        }
        System.out.println();
    }
}
