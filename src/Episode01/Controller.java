package Episode01;

import java.util.Scanner;
import java.util.Random;

public class Controller {
    static final int VALID_UNIT_SIZE = 3;
    static final int NUMBER_COUNT = 5;
    static final int MAX_OF_NUMBER = 100;
    static final int PERSON_COUNT = 5;
    static final int MAX_WEIGHT = 200;
    static final int MAX_HEIGHT = 250;

    public static void main() {
        //Print to standard output
        System.out.println("Hello World!");

        //Check divisibility and print result
        if (45 % 5 == 0) {
            System.out.println("45 IS divisible by 5");
        } else {
            System.out.println("45 IS NOT divisible by 5");
        }

        //------------------------------------------------------
        // Procedural programming - put (disjunct) subtasks into methods

        //Make it dynamic, data is read from console (standard input)
        int number;

        number = getPositiveIntFromConsole("Please enter a number!", 100);
        System.out.println("The number: " + number);

        checkParity(number);

        checkDivision(number, 5);
        checkDivision(number, 3);
        checkDivision(number, 7);

        checkNzp(number);

        // new method is created, because exit (acceptance) condition has changed
        int allocationSize = getAllocationSizeForUnits();
        System.out.println("Allocated unit count: " + (allocationSize / VALID_UNIT_SIZE));

        // Use recursion to calculate factorial and Fibonacci numbers

        int n = getPositiveIntFromConsole("Please enter n!", 10);
        System.out.println("With NO recursion");
        System.out.println(n + "!=" + calculateFactorial(n));
        System.out.println("Fib(" + n + ")=" + calculateFibonacci(n));
        System.out.println("With recursion");
        System.out.println(n + "!=" + calculateFactorialRecursive(n));
        System.out.println("Fib(" + n + ")=" + calculateFibonacciRecursive(n));

        //========== Arrays
        int[] numberArray = createFilledArray();
        printArray("Raw array", numberArray);
        numberArray = bubbleSortArray(numberArray);
        printArray("Sorted array", numberArray);

        //========== Loosely related data arrays
        int[] weightArray = new int[PERSON_COUNT];
        int[] heightArray = new int[PERSON_COUNT];
        int[][] arrays = new int[][] {weightArray, heightArray};

        for (int p = 0; p < PERSON_COUNT; p++) {
            System.out.println((p + 1) + ". person");
            weightArray[p] = getPositiveIntFromConsole(
                    "weight: ",
                    MAX_WEIGHT
            );
            heightArray[p] = getPositiveIntFromConsole(
                    "height: ",
                    MAX_HEIGHT
            );
        }

        printArrays("Original data (w; h)", arrays);
        bubbleSortArrays(weightArray, heightArray);
        printArrays("Sorted data (w; h)", arrays);

        //Fill person data arrays with random values
        getRandomValues(weightArray);
        getRandomValues(heightArray);

        //Create new random data for persons
        int[] age = new int[PERSON_COUNT];
        int[] shoeSize = new int[PERSON_COUNT];
        getRandomValues(age);
        getRandomValues(shoeSize);

        //Print original data.
        //Data list (array of arrays to print) is created in the method call
        printArrays("Original data (w; h)", new int[][] {weightArray, heightArray, age, shoeSize});

        //4 arrays describe a person, the connection between data is the array index.
        // ith elements represent the same person.
        // How to sort such data?
        // Should a new sorter method with 4 parameter be created?

        //Use indices instead!

        //Crate indices for original data
        int[] indices = new int[PERSON_COUNT];
        for(int i=0; i<indices.length; i++) {
            indices[i] = i;
        }

        //Sort key and indices
        bubbleSortArrays(weightArray, indices);

        //Reorder related data by sorted indices
        reorderArray(heightArray, indices);
        reorderArray(age, indices);
        reorderArray(shoeSize, indices);

        printArrays("Sorted data (w; h)", new int[][] {weightArray, heightArray, age, shoeSize});
    }

    private static void checkParity(int number) {
        messageOnDivision(
                number,
                2,
                "even",
                "odd"
        );
    }

    private static void checkDivision(int number, int divisor) {
        messageOnDivision(
                number,
                divisor,
                "divisible by " + divisor,
                "not divisible by " + divisor
        );
    }

    private static void messageOnDivision(int number, int divisor, String yesMessage, String noMessage) {
        if (number % divisor == 0) {
            System.out.println(yesMessage);
        } else {
            System.out.println(noMessage);
        }
    }

    private static void checkNzp(int number) {
        if (number < 0) {
            System.out.println("negative");
        } else {
            if (number > 0) {
                System.out.println("positive");
            } else {
                System.out.println("zero");
            }
        }
    }

    private static int getPositiveIntFromConsole(String message, int max) {
        System.out.print(message + " ");
        boolean isNotValid;
        int number;
        Scanner consoleScanner = new Scanner(System.in);
        do {
            number = consoleScanner.nextInt();

            isNotValid = number <= 0 || number > max;
            if (isNotValid) {
                System.out.print("Please try again! (0<x<=" + max + ") ");

            }
        } while (isNotValid);

        return number;
    }

    private static int getAllocationSizeForUnits() {
        Scanner consoleScanner = new Scanner(System.in);

        System.out.println("Enter allocation for " + VALID_UNIT_SIZE + " sized units!");
        boolean isNotValid;
        int number;
        do {
            number = consoleScanner.nextInt();

            isNotValid = number <= 0 || number % VALID_UNIT_SIZE != 0;
            if (isNotValid) {
                System.out.println("Enter another number! (positive, divisible by " + VALID_UNIT_SIZE + ")");
            }
        } while (isNotValid);

        return number;
    }

    private static int calculateFactorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }

        return factorial;
    }

    private static int calculateFactorialRecursive(int n) {
        if(n==1) return 1;

        return n * calculateFactorialRecursive(n-1);
    }

    private static int calculateFibonacci(int n) {
        int fm2 = 0;
        int fm1 = 1;
        int fibonacci = 1;

        for (int i = 1; i <= n - 1; i++) {
            fibonacci = fm2 + fm1;
            fm2 = fm1;
            fm1 = fibonacci;
        }

        return fibonacci;
    }

    private static int calculateFibonacciRecursive(int n) {
        if(n == 0) return 0;
        if(n == 1) return 1;

        return calculateFibonacciRecursive(n - 1) + calculateFibonacciRecursive(n - 2);
    }

    private static int[] createFilledArray() {
        int[] numberArray = new int[NUMBER_COUNT];
        System.out.println("Fill up an array of " + NUMBER_COUNT + " items");
        for (int i = 0; i < NUMBER_COUNT; i++) {
            numberArray[i] = getPositiveIntFromConsole(
                    "Enter #" + i + " element:",
                    MAX_OF_NUMBER
            );
        }

        return numberArray;
    }

    private static void getRandomValues(int[] array) {
        Random rnd = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rnd.nextInt();
        }
    }

    private static void printArray(String message, int[] array) {
        System.out.println("---------- " + message);
        for (int i = 0; i < array.length; i++) {
            System.out.println("#" + i + ": " + array[i]);
        }
    }

    private static int[] bubbleSortArray(int[] array) {
        boolean hasChange;
        int temp;
        do {
            hasChange = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;

                    hasChange = true;
                }
            }
        } while (hasChange);
        return array;
    }

    private static void printArrays(String message, int[][] array) {
        System.out.println("---------- " + message);
        for (int i = 0; i < array[0].length; i++) {

            System.out.print("#" + i + ": (");

            for (int j = 0; j < array.length; j++) {
                System.out.print(array[j][i]);

                if(j<array.length - 1)
                    System.out.println(", ");
            }
            System.out.println(")");
        }
    }

    private static void bubbleSortArrays(int[] array, int[] otherArray) {
        boolean hasChange;
        int temp;
        do {
            hasChange = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i+1]) {
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;

                    temp = otherArray[i];
                    otherArray[i] = otherArray[i+1];
                    otherArray[i+1] = temp;

                    hasChange = true;
                }
            }
        } while (hasChange);
    }

    private static void reorderArray(int[] array, int[] indices) {
        int[] unsortedArray = array.clone();
        for (int i = 0; i < indices.length - 1; i++) {
            array[i] = unsortedArray[indices[i]];
        }
    }
}
