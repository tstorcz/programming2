package Episode12;

import com.google.gson.annotations.SerializedName;

public class Track {
    private String name;

    private String src;

    @SerializedName(value = "trkseg")
    private TrackPoint[] trackPoints;

    private Vector[] trackSegments;

    public String getName() {
        return name;
    }

    public String getSrc() {
        return src;
    }

    public TrackPoint[] getTrackPoints() {
        return trackPoints;
    }

    public Vector[] getTrackSegments() { return trackSegments; }

    public void calculateTrackSegments() {
        trackSegments = new Vector[trackPoints.length - 1];
        for(int i = 0; i< trackPoints.length - 1; i++) {
            trackSegments[i] = new Vector(trackPoints[i], trackPoints[i + 1]);
        }
    }
}
