package Episode12;

public class PointCartesian {
    public static final PointCartesian ORIGIN = new PointCartesian(0, 0, 0);
    public static final double RAD_TO_DEG_COEFFICIENT =  180 / Math.PI;

    private final double x;
    private final double y;
    private final double z;
    private PointSpherical pointSpherical;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public PointSpherical getSpherical() {
        if(pointSpherical == null)
            pointSpherical = getSpherical(this);
        return pointSpherical;
    }

    public PointCartesian(double x, double y, double z, PointSpherical pointSpherical) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.pointSpherical = pointSpherical;
    }

    public PointCartesian(double x, double y, double z) {
        this(x, y, z, null);
    }

    public PointCartesian getVectorToPoint(PointCartesian point) {
        return new PointCartesian(point.getX() - x, point.getY() - y, point.getZ() - z);
    }

    public PointCartesian shift(PointCartesian point) {
        return new PointCartesian(x + point.getX(), y + point.getY(), z + point.getZ());
    }

    public PointCartesian invert() {
        return new PointCartesian(-x, -y, -z);
    }

    public double getDistance(PointCartesian point) {
        return Math.sqrt(Math.pow(x - point.getX(), 2) + Math.pow(y - point.getY(), 2) + Math.pow(z - point.getZ(), 2));
    }

    public static PointSpherical getSpherical(PointCartesian point) {
        //Calculate geodetic latitude, longitude, and altitude above planetary ellipsoid from Earth-centered Earth-fixed (ECEF) position
        //https://de.mathworks.com/help/aeroblks/ecefpositiontolla.html?s_tid=doc_ta
        double r = point.getDistance(ORIGIN);

        double lon = Math.atan2(point.getY(), point.getX()) * RAD_TO_DEG_COEFFICIENT;
        double lat = Math.asin(point.getZ()/r) * RAD_TO_DEG_COEFFICIENT;
        double elev = r - PointSpherical.EARTH_EQUATORIAL_RADIUS;

        //double s = Math.sqrt(point.getX() * point.getX() + point.getY() * point.getY());
        //double lat_reduced = Math.atan2(point.getZ(), (1 - PointSpherical.EARTH_FLATTENING) * s);

        return new PointSpherical(lat, lon, elev, point);
    }
}
