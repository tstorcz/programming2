package Episode12;

public class GPSPosition extends PointSpherical {
    private final double speed;

    public GPSPosition(double lat, double lon, double elevation, double speed) {
        super(lat, lon, elevation);
        this.speed = speed;
    }

    public double getSpeed() { return speed; }
}
