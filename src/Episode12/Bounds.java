package Episode12;

import com.google.gson.annotations.SerializedName;

public class Bounds {
    @SerializedName(value = "minlat")
    private double minLat;

    @SerializedName(value = "maxlat")
    private double maxLat;

    @SerializedName(value = "minlon")
    private double minLon;

    @SerializedName(value = "maxlon")
    private double maxLon;

    public double getMinLat() {
        return minLat;
    }

    public double getMaxLat() {
        return maxLat;
    }

    public double getMinLon() {
        return minLon;
    }

    public double getMaxLon() {
        return maxLon;
    }
}
