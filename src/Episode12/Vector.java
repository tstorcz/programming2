package Episode12;

public class Vector {
    private final PointSpherical point1;
    private final PointSpherical point2;

    private final PointCartesian vector;

    public PointSpherical getPoint1() {
        return point1;
    }

    public PointSpherical getPoint2() {
        return point2;
    }

    public PointCartesian getRelativeVector() {return vector; }

    public double getLength() {
        return point1.getDistance(point2);
    }

    public Vector(PointSpherical point1, PointSpherical point2) {
        this.point1 = point1;
        this.point2 = point2;
        this.vector = point1.getCartesian().getVectorToPoint(this.point2.getCartesian());
    }

    public double getDistance(PointSpherical point) {
        double c = getLength();
        double a = point1.getDistance(point);
        double b = point2.getDistance(point);

        double ma = Math.sqrt((a+b+c)*(-a+b+c)*(a-b+c)*(a+b-c)) / (2 * a);
        double alpha = Math.acos((-a*a+b*b+c*c)/(2*b*c)) * PointCartesian.RAD_TO_DEG_COEFFICIENT;
        double beta = Math.acos((a*a-b*b+c*c)/(2*a*c)) * PointCartesian.RAD_TO_DEG_COEFFICIENT;

        //if angle is obtuse (>90) return the distance to the vertex
        if(alpha > 90)return b;
        if(beta > 90)return a;

        return ma;
    }

    public double getAngle(Vector other) {
        return Math.acos(this.scalarProduct(other)/(this.getLength() * other.getLength())) * (180 / Math.PI);
    }

    public double scalarProduct(Vector other) {
        PointCartesian a = this.vector;
        PointCartesian b = other.vector;

        return a.getX() * b.getX() + a.getY() * b.getY() + a.getZ() + b.getZ();
    }
}
