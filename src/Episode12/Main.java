package Episode12;

import java.io.IOException;

public class Main implements PathCallbacks {
    public void start() throws IOException {
        Path path = PathFactory.createFromJson("Data\\Ep12\\2023_NN_Ultrabalaton.json", this);

        PointSpherical p = path.getTrack().getTrackPoints()[0];
        PointCartesian pc = p.getCartesian();
        PointSpherical p2 = pc.getSpherical();
    }

    @Override
    public void trackPointChecked(TrackPoint tp) {
        System.out.println("TrackPoint checked: " + tp.toString());
    }

    @Override
    public void wayPointChecked(WayPoint wp) {
        System.out.println("WayPoint checked: " + wp.toString());
    }

    @Override
    public void trackPointCheckedWithSkip(TrackPoint tp) {
        System.out.println("TrackPoint checked while skipping others: " + tp.toString());
    }

    @Override
    public void wayPointCheckedWithSkip(WayPoint wp) {
        System.out.println("WayPoint checked while skipping others: " + wp.toString());
    }

    @Override
    public void tooFarFromTrack(double distanceFromTrack) {
        System.out.println("You are too far from the track (" + distanceFromTrack + ")!");
    }
}
