package Episode12;

public interface PathCallbacks {
    void trackPointChecked(TrackPoint tp);
    void wayPointChecked(WayPoint wp);

    void trackPointCheckedWithSkip(TrackPoint tp);
    void wayPointCheckedWithSkip(WayPoint wp);

    void tooFarFromTrack(double distanceFromTrack);
}
