package Episode12;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class PathFactory {
    public static Path createFromJson(String fileName, PathCallbacks callbacks) throws FileNotFoundException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonReader reader = new JsonReader(new FileReader(fileName));
        Path path = gson.fromJson(reader, Path.class);
        path.initialize(callbacks);

        return path;
    }
}
