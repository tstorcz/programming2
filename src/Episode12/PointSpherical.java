package Episode12;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class PointSpherical implements Comparable<PointSpherical> {
    public static final PointSpherical ORIGIN = new PointSpherical(0, 0, 0);
    public static final double EARTH_EQUATORIAL_RADIUS = 6378137;
    public static final double EARTH_FLATTENING = 1/298.257223563;
    public static final double EARTH_FLATTENING_COEFFICIENT = 1; //Math.pow(1-EARTH_FLATTENING, 2);
    public static final double DEG_TO_RAD_COEFFICIENT = Math.PI / 180;

    private final double lat;
    private final double lon;
    @SerializedName(value = "ele")
    private final double elevation;

    transient private LocalDateTime checked;

    transient private double distanceTo = 0;
    transient private PointCartesian pointCartesian;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getElevation() {
        return elevation;
    }

    public double getDistanceTo() {
        return distanceTo;
    }

    public void setDistanceTo(double distanceTo) {
        this.distanceTo = distanceTo;
    }

    public PointCartesian getCartesian() {
        if(pointCartesian == null){
            pointCartesian = getCartesian(this);
        }
        return pointCartesian;
    }

    public PointSpherical(double lat, double lon, double elevation, PointCartesian pointCartesian) {
        this.lat = lat;
        this.lon = lon;
        this.elevation = elevation;
        this.pointCartesian = pointCartesian;
        this.checked = null;
    }

    public PointSpherical(double lat, double lon, double elevation) {
        this(lat, lon, elevation, null);
    }

    public double getDistance(PointSpherical point) {
        double dLat = (point.getLat()-this.getLat()) * DEG_TO_RAD_COEFFICIENT;
        double dLon = (point.getLon()-this.getLon()) * DEG_TO_RAD_COEFFICIENT;
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(this.getLat() * DEG_TO_RAD_COEFFICIENT) * Math.cos(point.getLat() * DEG_TO_RAD_COEFFICIENT) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return EARTH_EQUATORIAL_RADIUS * c;
    }

    public static PointCartesian getCartesian(PointSpherical point) {
        //Calculate Earth-centered Earth-fixed (ECEF) position from geodetic latitude, longitude, and altitude above planetary ellipsoid
        //https://de.mathworks.com/help/aeroblks/llatoecefposition.html
        double lat = point.getLat() * DEG_TO_RAD_COEFFICIENT;
        double lng = point.getLon() * DEG_TO_RAD_COEFFICIENT;

        double lat_msl = Math.atan(EARTH_FLATTENING_COEFFICIENT * Math.tan(lat));    //geocentric latitude on mean sea-level
        double surface_radius =                                                      //radius at surface point
                Math.sqrt(
                        Math.pow(EARTH_EQUATORIAL_RADIUS, 2)
                        /
                        (1 + (1 / EARTH_FLATTENING_COEFFICIENT - 1) * Math.pow(Math.sin(lat_msl), 2))
                );

        double x = surface_radius * Math.cos(lat_msl) * Math.cos(lng) + point.getElevation() * Math.cos(lat) * Math.cos(lng);
        double y = surface_radius * Math.cos(lat_msl) * Math.sin(lng) + point.getElevation() * Math.cos(lat) * Math.sin(lng);
        double z = surface_radius * Math.sin(lat_msl) + point.getElevation() * Math.sin(lat);

        return new PointCartesian(x, y, z, point);
    }

    public LocalDateTime getChecked() { return checked; }

    public void check()  {
        if (checked == null) {
            checked = LocalDateTime.now();
        }
    }

    public boolean isChecked() { return checked != null; }

    @Override
    public int compareTo(PointSpherical o) {
        if(lat == o.getLat() && lon == o.getLon())
            return 0;

        return Double.compare(lat, o.getLat()) == 0 ? Double.compare(lon, o.getLon()) : Double.compare(lat, o.getLat());
    }

    @Override
    public String toString() {
        return "PointSpherical{" +
                "lat=" + lat +
                ", lon=" + lon +
                ", elevation=" + elevation +
                ", checked=" + checked +
                ", distanceTo=" + distanceTo +
                '}';
    }
}
