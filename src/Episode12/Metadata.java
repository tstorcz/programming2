package Episode12;

public class Metadata {
    private String name;

    private String desc;

    private Link link;

    private Bounds bounds;

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Link getLink() {
        return link;
    }

    public Bounds getBounds() {
        return bounds;
    }
}
