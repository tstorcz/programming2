package Episode12;

public class WayPoint extends PointSpherical {
    private String name;

    private String desc;

    private Boolean isMarked;

    transient private TrackPoint trackPoint;

    public WayPoint(double lat, double lon, double elevation) {
        super(lat, lon, elevation);
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Boolean getMarked() {
        return isMarked;
    }

    public void setMarked(Boolean marked) {
        isMarked = marked;
    }

    public TrackPoint getTrackPoint() {
        return trackPoint;
    }

    public void setTrackPoint(TrackPoint trackPoint) {
        this.trackPoint = trackPoint;
    }

    @Override
    public String toString() {
        return "WayPoint{" +
                super.toString() +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", isMarked=" + isMarked +
                ", trackPoint=" + trackPoint +
                '}';
    }
}
