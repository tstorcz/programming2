package Episode12;

public class Link {
    private String href;

    private String text;

    public String getHref() {
        return href;
    }

    public String getText() {
        return text;
    }
}
