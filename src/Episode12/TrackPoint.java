package Episode12;

public class TrackPoint extends PointSpherical {
    private double distanceFrom = 0;
    private double totalDistance = 0;

    transient private WayPoint wayPoint;

    public TrackPoint(double lat, double lon, double elevation) {
        super(lat, lon, elevation);
    }

    public void setWayPoint(WayPoint wayPoint) {
        this.wayPoint = wayPoint;
    }

    public WayPoint getWayPoint() {
        return wayPoint;
    }

    public double getDistanceFrom() {
        return distanceFrom;
    }

    public void setDistanceFrom(double distanceFrom) {
        this.distanceFrom = distanceFrom;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    @Override
    public String toString() {
        return "TrackPoint{" +
                super.toString() +
                ", distanceFrom=" + distanceFrom +
                ", totalDistance=" + totalDistance +
                ", wayPoint=" + wayPoint +
                "}";
    }
}
