package Episode12;

import com.google.gson.annotations.SerializedName;

public class Path {
    private String version;
    private String creator;
    private String schemaLocation;

    private Metadata metadata;

    @SerializedName(value = "wpt")
    private WayPoint[] wayPoints;

    @SerializedName(value = "trk")
    private Track track;

    private double trackPointCheckDistance;
    private double wayPointCheckDistance;

    private double maxDistanceFromTrack;
    private boolean trackPointSkipEnabled;
    private boolean wayPointSkipEnabled;

    transient private int lastWayPointIndex;
    transient private int lastTrackPointIndex;
    transient private GPSPosition lastPosition;

    transient PathCallbacks callbacks;

    public String getVersion() {
        return version;
    }

    public String getCreator() {
        return creator;
    }

    public String getSchemaLocation() {
        return schemaLocation;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public WayPoint[] getWayPoints() {
        return wayPoints;
    }

    public Track getTrack() {
        return track;
    }

    public WayPoint getWayPoint(int shift)
    {
        return wayPoints[lastWayPointIndex + shift];
    }

    public TrackPoint getTrackPoint(int shift)
    {
        return track.getTrackPoints()[lastTrackPointIndex + shift];
    }

    public Vector getTrackVector(int shift)
    {
        return getTrack().getTrackSegments()[lastTrackPointIndex + shift];
    }

    public GPSPosition getLastPosition(){ return lastPosition; }

    public void initialize(PathCallbacks callbacks) {
        calculateDistances();
        track.calculateTrackSegments();
        lastWayPointIndex = 0;
        lastTrackPointIndex = 0;
        lastPosition = null;
        this.callbacks = callbacks;
    }

    private void calculateDistances() {
        for(WayPoint wp : this.getWayPoints()) {
            double minDistance = 0;
            TrackPoint closest = null;
            for(TrackPoint tp : track.getTrackPoints()) {
                if(closest == null || tp.getDistance(wp) < minDistance) {
                    minDistance = tp.getDistance(wp);
                    closest = tp;
                }
            }
            if(closest != null) {
                closest.setWayPoint(wp);
                wp.setTrackPoint(closest);
            }
        }

        double stepDistance;
        double sectorDistance = 0;
        double totalDistance = 0;
        TrackPoint prev = track.getTrackPoints()[0];
        for(TrackPoint tp : track.getTrackPoints()) {
            stepDistance = prev.getDistance(tp);
            sectorDistance += stepDistance;
            totalDistance += stepDistance;

            tp.setDistanceFrom(sectorDistance);
            tp.setTotalDistance(totalDistance);

            if(tp.getWayPoint() != null) {
                sectorDistance = 0;
            }
            prev = tp;
        }

        double distance = track.getTrackPoints()[this.getTrack().getTrackPoints().length - 1].getDistanceFrom();
        for(int i=track.getTrackPoints().length - 1; i >= 0; i--) {
            TrackPoint tp = this.getTrack().getTrackPoints()[i];
            tp.setDistanceTo(distance - tp.getDistanceFrom());
            if(tp.getWayPoint() != null) {
                tp.getWayPoint().setDistanceTo(distance);
                distance = tp.getDistanceFrom();
            }
        }
    }

    public double distanceFromTrack(PointSpherical point) {
        if(point == null)return 0;

        double min = -1;
        double d;

        for(Vector segment : track.getTrackSegments()) {
            d = segment.getDistance(point);
            if(min < 0 || d < min)min = d;
        }

        return min;
    }

    public TrackPoint closestTrackPoint(PointSpherical point) {
        if(point == null) return null;

        double min = 0;
        double d;
        TrackPoint closest = null;

        for(TrackPoint tp : track.getTrackPoints()) {
            d = tp.getDistance(point);
            if(closest == null || d < min) {
                min = d;
                closest = tp;
            }
        }

        return closest;
    }

    private int getTrackPointIndex(TrackPoint tp) {
        for(int i=0; i<track.getTrackPoints().length; i++){
            if(track.getTrackPoints()[i].compareTo(tp) == 0) {
                return i;
            }
        }
        return -1;
    }

    private int getWayPointIndex(WayPoint wp) {
        for(int i=0; i<wayPoints.length; i++){
            if(wayPoints[i].compareTo(wp) == 0) {
                return i;
            }
        }
        return -1;
    }

    public WayPoint closestWayPoint(PointSpherical point) {
        if(point == null) return null;

        double min = 0;
        double d;
        WayPoint closest = null;

        for(WayPoint wp : wayPoints) {
            d = wp.getDistance(point);
            if(closest == null || d < min) {
                min = d;
                closest = wp;
            }
        }

        return closest;
    }

    public void setLastPosition(GPSPosition newPosition) {
        WayPoint wp = null;
        int idx;

        TrackPoint tp = closestTrackPoint(lastPosition);
        if(tp != null && tp.getDistance(lastPosition) <= trackPointCheckDistance) {
            if(!tp.isChecked()) {
                idx = getTrackPointIndex(tp);
                if(idx == lastTrackPointIndex + 1) {
                    tp.check();
                    wp = tp.getWayPoint();
                    callbacks.trackPointChecked(tp);
                }
                else {
                    callbacks.trackPointCheckedWithSkip(tp);
                }
            }
        }

        if(wp == null) wp = closestWayPoint(lastPosition);
        if(wp != null && wp.getDistance(lastPosition) <= wayPointCheckDistance) {
            if(!wp.isChecked()) {
                idx = getWayPointIndex(wp);
                if(idx == lastWayPointIndex + 1) {
                    wp.check();
                    if (lastWayPointIndex < wayPoints.length - 1) {
                        lastWayPointIndex += 1;
                    }
                    callbacks.wayPointChecked(wp);
                }
                else {
                    callbacks.wayPointCheckedWithSkip(wp);
                }
            }
        }

        double distanceFromTrack = distanceFromTrack(newPosition);
        if(distanceFromTrack > maxDistanceFromTrack) {
            callbacks.tooFarFromTrack(distanceFromTrack);
        }

        lastPosition = newPosition;
    }
}
