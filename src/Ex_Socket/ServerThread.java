package Ex_Socket;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class ServerThread extends Thread {
    private Socket socket;
    private ArrayList<String> content;
    private int contentLength;
    private int bufferSize;

    public ServerThread(Socket socket, int bufferSize) {
        this.socket = socket;
        this.bufferSize = bufferSize;

        content = new ArrayList<>();
        contentLength = 0;
    }

    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            String text;
            boolean listen = true;
            String lines[];

            do {
                text = reader.readLine();

                content.add(text);
                contentLength += text.length();

                if(contentLength >= bufferSize) {
                    writeBuffer();
                }

                lines = text.split("\n");
                for(String l : lines) {
                    listen &= !l.equals("bye");
                }
            } while (listen);

            socket.close();
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }

        try {
            writeBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeBuffer() throws IOException {
        BufferedWriter output = new BufferedWriter(new FileWriter("my_file_name", true));
        for(String s : content) {
            output.write(s);
        }
        content.clear();
        contentLength = 0;
    }
}
