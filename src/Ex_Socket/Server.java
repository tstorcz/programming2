package Ex_Socket;

import java.io.*;
import java.net.*;

public class Server {
    private int port;
    private int bufferSize;

    public Server(int port, int bufferSize) {
        this.port = port;
        this.bufferSize = bufferSize;
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Server is listening on port " + port + "...");

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected " + socket.getRemoteSocketAddress().toString());

                new ServerThread(socket, bufferSize).start();
            }

        } catch (IOException ex) {
            System.out.println("Exception occurred: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}

