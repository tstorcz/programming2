package Episode02;

public class Person {
    public static final int WEIGHT_MIN = 40;
    public static final int WEIGHT_MAX = 120;
    public static final int HEIGHT_MIN = 140;
    public static final int HEIGHT_MAX = 210;
    public static final int BIRTH_YEAR_MIN = 1900;
    public static final int BIRTH_YEAR_MAX = 2015;

    //Data members are private to provide abstract interface
    private int height;
    private int weight;
    private int birthYear;

    //Getters and setters are provided to read and modify data
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if(height >= HEIGHT_MIN && height <= HEIGHT_MAX) {
            this.height = height;
        }
        else {
            System.out.println("Invalid height");
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if(weight >= WEIGHT_MIN && weight <= WEIGHT_MAX) {
            this.weight = weight;
        }
        else {
            System.out.println("Invalid weight");
        }
    }

    //No setter for birth year, because it is not subject of change
    public int getBirthYear() {
        return birthYear;
    }

    //Constructor helps to create a valid person
    public Person(int height, int weight, int birthYear) {
        if(height >= HEIGHT_MIN && height <= HEIGHT_MAX
                && weight >= WEIGHT_MIN  && weight <= WEIGHT_MAX
                && birthYear >= BIRTH_YEAR_MIN && birthYear <= BIRTH_YEAR_MAX) {
            this.height = height;
            this.weight = weight;
            this.birthYear = birthYear;
        }
        else {
            System.out.println("Person initialization error!!!");
        }
    }

    public double calculateBMI() {
        return 100.0 * weight / height;
    }

    public String getString() {
        return " weight: " + getWeight()
                + " height: " + getHeight()
                + " birth year: " + getBirthYear();
    }

}
