package Episode02;

public class Controller {
    private static final int NUMBER_OF_PERSONS = 3;

    public static void main() {
        PersonStorage personStorage = new PersonStorage(NUMBER_OF_PERSONS);

        System.out.println("Size of array to store persons: " + personStorage.getNumberOfPerson());

        System.out.println("Person on 3rd position: " + personStorage.getPerson(2));

        //personArray.readPersonsFromConsole();
        //personArray.randomPersons();
        personStorage.readPersonsFromFile("Data\\Ep02\\Ep02_Persons.csv");

        Person third = personStorage.getPerson(2);
        third.setHeight(250);
        System.out.println("3. person -"
                + " weight: " + third.getWeight()
                + " height: " + third.getHeight()
        );
        System.out.println("3. person -" + third.getString());

        System.out.println("Original list of persons:");
        personStorage.printPersons();

        System.out.println("Original list-2 of persons:");
        personStorage.printPersons2();

        personStorage.sortPersons();
        System.out.println("Sorted list of persons:");
        personStorage.printPersons();

        System.out.println("BMI list of persons:");
        printBMI(personStorage);
    }

    private static void printBMI(PersonStorage personStorage) {
        for (int i = 0; i < personStorage.getNumberOfPerson(); i++) {
            System.out.println(
                    String.format(
                            "BMI of person #%d: %.1f",
                            i + 1,
                            personStorage.getPerson(i).calculateBMI()
                    )
            );
        }
    }
}
