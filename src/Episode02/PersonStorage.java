package Episode02;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class PersonStorage {

    //Data member is private to provide abstract interface
    private Person[] persons;
    private Random rnd;

    //Methods to access information stored in the list
    // there is no setter for the list, it is not subject of change
    // there is no getter for the list WHY???
    public int getNumberOfPerson() {
        return persons.length;
    }

    public Person getPerson(int index) {
        return persons[index];
    }

    //Constructor helps to create a valid, but initially empty array
    public PersonStorage(int numberOfPersons) {
        persons = new Person[numberOfPersons];
        rnd = new Random();
    }

    //Methods to fill array
    public void readPersonsFromConsole() {
        int weight, height, birth_year;

        for(int i=0;i<persons.length; i++) {
            // These two lines result the same: Print to the console
            // System.out.println(String.format("Person #%d", i + 1));
            System.out.printf("Person #%d%n", i + 1);

            System.out.print("Weight: ");
            weight = getNumberFromConsole(Person.WEIGHT_MIN, Person.WEIGHT_MAX);

            System.out.print("Height: ");
            height = getNumberFromConsole(Person.HEIGHT_MIN, Person.HEIGHT_MAX);

            System.out.print("Birth year: ");
            birth_year = getNumberFromConsole(Person.BIRTH_YEAR_MIN, Person.BIRTH_YEAR_MAX);

            persons[i] = new Person(weight, height, birth_year);
        }
    }

    public void randomPersons() {
        for (int i = 0; i < persons.length; i++) {
            persons[i] = new Person(
                    getRandomNumber(Person.WEIGHT_MIN, Person.WEIGHT_MAX),
                    getRandomNumber(Person.HEIGHT_MIN, Person.HEIGHT_MAX),
                    getRandomNumber(Person.BIRTH_YEAR_MIN, Person.BIRTH_YEAR_MAX)
            );
        }
    }

    public void readPersonsFromFile(String fileName) {
        File myFile = new File(fileName);
        Scanner myReader = null;

        try {
            myReader = new Scanner(myFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        int i = 0;
        while (myReader.hasNextLine() && i< getNumberOfPerson()) {
            String data = myReader.nextLine();
            String[] numberTexts = data.split(";");
            persons[i++] = new Person(
                    Integer.parseInt(numberTexts[0]),
                    Integer.parseInt(numberTexts[1]),
                    Integer.parseInt(numberTexts[2])
            );
        }
        myReader.close();
    }

    //Both method print stored data
    //Which one you prefer? Why?
    public void printPersons() {
        for (int i=0;i<persons.length;i++) {
            System.out.println((i+1)+". person -"
                    + " weight: " + persons[i].getWeight()
                    + " height: " + persons[i].getHeight()
            );
        }
    }

    public void printPersons2() {
        for (int i=0;i<persons.length;i++) {
            System.out.println((i+1)+". person -" + persons[i].getString());
        }
    }

    //Sort by height
    public void sortPersons() {
        boolean hasSwap;
        do {
            hasSwap = false;
            for (int i = 0; i < persons.length - 1; i++) {
                if (persons[i].getHeight() > persons[i + 1].getHeight()) {
                    Person temp = persons[i];
                    persons[i] = persons[i + 1];
                    persons[i + 1] = temp;

                    hasSwap = true;
                }
            }
        } while (hasSwap);
    }

    //Private, because Class should be responsible for management of array of persons
    // and should not provide console functions
    private int getNumberFromConsole(int lowerBorder, int upperBorder) {
        Scanner consoleScanner = new Scanner(System.in);
        int number;

        do {
            number = consoleScanner.nextInt();
        }while(lowerBorder > number || number > upperBorder);

        return number;
    }

    //Private, because Class should be responsible for management of array of persons
    // and should not provide random number generation functions
    private int getRandomNumber(int lowerBorder, int upperBorder) {
        return rnd.nextInt(upperBorder - lowerBorder) + lowerBorder;
    }
}
