package Episode05;

public class Additive {
    public static int add(int op1, int op2) {
        return op1 + op2;
    }

    public static float add(float op1, float op2) {
        return op1 + op2;
    }

    public static double add(double op1, double op2) {
        return op1 + op2;
    }

    public static String add(String op1, String op2) {
        return op1 + op2;
    }

    public static Data add(Data op1, Data op2) {
        return new Data(op1.getData() + op2.getData());
    }
}
