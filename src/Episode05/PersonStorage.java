package Episode05;

import java.util.ArrayList;
import java.util.Comparator;

public class PersonStorage {
    private ArrayList<Person> persons;

    public void add(Person newPerson){
        persons.add(newPerson);
    }

    public Person get(int index) {
        return persons.get(index);
    }

    public int size() {
        return persons.size();
    }

    public PersonStorage() {
        persons = new ArrayList<>();
    }

    public void sortByFirstName() {
        persons.sort((p1, p2) -> p1.getFirstName().compareToIgnoreCase(p2.getFirstName()));
    }

    public void sortByLastName() {
        persons.sort((p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName()));
    }

    public void sortByName() {
        persons.sort(
                (p1, p2) -> {
                    int lncr = p1.getLastName().compareToIgnoreCase(p2.getLastName());
                    if(lncr != 0) return lncr;
                    return p1.getFirstName().compareToIgnoreCase(p2.getFirstName());
                }
        );
    }

    public void sortByAge() {
        persons.sort(Comparator.comparingInt(Person::getBirthYear));
    }
}
