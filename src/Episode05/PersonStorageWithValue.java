package Episode05;

import java.util.Comparator;

//Please note that type parameter of superclass can be given constantly in class declaration
//The derived class is not generic, works only with Person
public class PersonStorageWithValue<T extends Person> extends DataStorage<T> {
    //Please note that 'items' in DataStorage is protected,
    //so this is accessible in subclasses of DataStorage, like this
    //and 'value' handling is inherited from DataStorage

    public void sortByFirstName() {
        items.sort(
                (p1, p2) -> p1.getFirstName().compareToIgnoreCase(p2.getFirstName())
        );
    }

    public void sortByLastName() {
        items.sort(
                (p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName())
        );
    }

    public void sortByName() {
        items.sort(
                (p1, p2) -> {
                    int lncr = p1.getLastName().compareToIgnoreCase(p2.getLastName());

                    if(lncr != 0) return lncr;

                    return p1.getFirstName().compareToIgnoreCase(p2.getFirstName());
                }
        );
    }

    public void sortByAge() {
        items.sort(
                Comparator.comparingInt(Person::getBirthYear)
        );
    }
}
