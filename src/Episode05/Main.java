package Episode05;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Random;

public class Main {

    private static final int ITEM_COUNT = 5;

    public void main() {
        usingOverloads();

        usingGenericArray();

        handlingCustomClasses();

        extendDataFunctionality();

        personInDataStorage();

        extendStorageFunctionality();

        handlePersonWithValue();

        moreStorages();
    }

    private void usingOverloads() {
        //General add operation with overload

        int iop1 = 4;
        int iop2 = 6;
        System.out.println(Additive.add(iop1, iop2));

        double dop1 = 4.4;
        double dop2 = 6.3;
        System.out.println(Additive.add(dop1, dop2));

        String sop1 = "Hello";
        String sop2 = "World";
        System.out.println(Additive.add(Additive.add(sop1, " "), sop2));

        //Can work with custom class (if class is additive)
        Data cop1 = new Data(12);
        Data cop2 = new Data(16);
        System.out.println(Additive.add(cop1, cop2));
    }

    private void usingGenericArray() {
        ArrayList<Integer> ints;
        ints = new ArrayList<>();

        fillInts(ints);

        printInts("unsorted", ints);

        Collections.sort(ints);

        printInts("sorted", ints);
    }

    private void fillInts(ArrayList<Integer> ints) {
        Random rnd = new Random();
        for(int i=0; i<ITEM_COUNT; i++) {
            ints.add(rnd.nextInt());
        }
    }

    private void printInts(String message, ArrayList<Integer> ints) {
        System.out.printf("\n!!!! %s%n", message);
        for(int i=0; i<ints.size(); i++) {
            System.out.println(ints.get(i));
        }
    }

    private void handlingCustomClasses() {
        //Generic storage for custom class
        //Custom operands
        Data cop1 = new Data(12);
        Data cop2 = new Data(16);
        Data cop3 = new Data(13);
        Data cop4 = new Data(15);

        ArrayList<Data> myData = new ArrayList<>();
        myData.add(cop1);
        myData.add(cop2);
        myData.add(cop3);
        myData.add(cop4);
        Data d = myData.get(0);

        //Custom storage for custom class
        //Extending functionality of ArrayList with sum and predefined sorts
        DataStorage<Data> dataStorage = new DataStorage<>();
        dataStorage.add(cop1);
        dataStorage.add(cop2);
        dataStorage.add(cop3);
        dataStorage.add(cop4);
        Data d1 = dataStorage.get(0);

        //Custom operations on custom storage
        printDataTyped("original", dataStorage);

        dataStorage.sort1();

        printDataTyped("sorted 1", dataStorage);

        dataStorage.sort2();

        printData("sorted 2", dataStorage);

        dataStorage.sortReversed();

        printData("reversed", dataStorage);
    }

    private void printDataTyped(String message, DataStorage<Data> storage) {
        System.out.println();
        System.out.printf(">>> data items %s%n", message);
        for (int i=0; i<storage.size(); i++) {
            System.out.println(storage.get(i));
        }
        System.out.printf("Sum: %d%n", storage.sum());
    }

    private void extendDataFunctionality() {
        ArrayList<Person> personStorage = new ArrayList<>();
        personStorage.add(new Person("John", "Killerman", 1983, 1000));
        personStorage.add(new Person("John", "Doe", 1980, 1));
        personStorage.add(new Person("John", "Smith", 1982, 10));
        personStorage.add(new Person("Jane", "Doe", 1985, 100));

        printPersons("original list", personStorage);

        personStorage.sort(
                (p1, p2) -> p1.getFirstName().compareToIgnoreCase(p2.getFirstName())
        );
        printPersons(" sorted by first name", personStorage);

        personStorage.sort(
                (p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName())
        );
        printPersons(" sorted by last name", personStorage);

        personStorage.sort(
                (p1, p2) -> {
                    int lncr = p1.getLastName().compareToIgnoreCase(p2.getLastName());
                    if(lncr != 0) return lncr;
                    return p1.getFirstName().compareToIgnoreCase(p2.getFirstName());
                }
        );
        printPersons(" sorted by last then first name", personStorage);

        personStorage.sort(
                (p1, p2) -> Integer.compare(p2.getBirthYear(), p1.getBirthYear())
        );
        printPersons(" sorted by birth year descending", personStorage);
    }

    private void printPersons(String message, ArrayList<Person> personStorage) {
        System.out.println();
        System.out.printf(">>> persons: %s%n", message);
        int sum = 0;
        for (Person p: personStorage) {
            System.out.println(p);
            sum += p.getData();
        }
        System.out.printf("Sum: %d%n", sum);
    }

    private void personInDataStorage() {
        DataStorage<Person> personStorage = new DataStorage<>();
        personStorage.add(new Person("John", "Killerman", 1983, 1000));
        personStorage.add(new Person("John", "Doe", 1980, 1));
        personStorage.add(new Person("John", "Smith", 1982, 10));
        personStorage.add(new Person("Jane", "Doe", 1985, 100));
        Person p = personStorage.get(0);

        //printDataTyped("persons as Data", personStorage);

        printData("original list", personStorage);

        personStorage.sort1();
        printData(" sorted by data", personStorage);

        //ArrayList of storage is hidden, can not be sorted from outside the class
        //persons can not be sorted by name, unless implemented out of the class
    }

    private <T extends Data> void printData(String message, DataStorage<T> storage) {
        System.out.println();
        System.out.printf(">>> data items %s%n", message);
        for (int i=0; i<storage.size(); i++) {
            System.out.println(storage.get(i));
        }
        System.out.printf("Sum: %d%n", storage.sum());
    }

    private void extendStorageFunctionality() {
        PersonStorage personStorage = new PersonStorage();

        personStorage.add(new Person("John", "Killerman", 1983, 1000));
        personStorage.add(new Person("John", "Doe", 1980, 1));
        personStorage.add(new Person("John", "Smith", 1982, 10));
        personStorage.add(new Person("Jane", "Doe", 1985, 100));
        printPersonsStorage("original list", personStorage);

        personStorage.sortByFirstName();
        printPersonsStorage(" sorted by first name", personStorage);

        personStorage.sortByLastName();
        printPersonsStorage(" sorted by last name", personStorage);

        personStorage.sortByName();
        printPersonsStorage(" sorted by name (last then first name)", personStorage);

        personStorage.sortByAge();
        printPersonsStorage(" sorted by birth year descending", personStorage);

        //ArrayList of storage is hidden, can not be sorted from outside the class
        //Sort by value has to be implemented
    }

    private void printPersonsStorage(String message, PersonStorage personStorage) {
        System.out.println();
        System.out.printf(">>> persons: %s%n", message);
        for (int i = 0; i < personStorage.size(); i++) {
            System.out.println(personStorage.get(i));
        }
    }

    private void handlePersonWithValue() {
        //Hide data stored in storage class, publish only an interface
        PersonStorageWithValue personStorage = new PersonStorageWithValue();

        personStorage.add(new Person("John", "Killerman", 1983, 1000));
        personStorage.add(new Person("John", "Doe", 1980, 1));
        personStorage.add(new Person("John", "Smith", 1982, 10));
        personStorage.add(new Person("Jane", "Doe", 1985, 100));

        printPersonsWithValueStorage("original list", personStorage);

        personStorage.sortByFirstName();
        printPersonsWithValueStorage(" sorted by first name", personStorage);

        personStorage.sortByLastName();
        printData(" sorted by last name", personStorage);

        personStorage.sortByName();
        printData(" sorted by name (last then first name)", personStorage);

        personStorage.sortByAge();
        printData(" sorted by birth year descending", personStorage);

        //Persons can be sorted by value
        personStorage.sort1();
        printData(" sorted by last name", personStorage);

        //PrintData method can be used, because PersonStorage has the interface of DataStorage (inherited)
        //All Person related data is printed (not just value) because toString is overridden in Person
        //therefore when accessing a Person type object via Data interface,
        //the overridden toString method will be called (in Person, not in Data)
    }

    private void printPersonsWithValueStorage(String message, PersonStorageWithValue personStorage) {
        System.out.println();
        System.out.printf(">>> persons: %s%n", message);
        for (int i = 0; i < personStorage.size(); i++) {
            System.out.println(personStorage.get(i));
        }
    }

    private void moreStorages() {
        PersonStorageWithValue<Person> persons = new PersonStorageWithValue<>();
        persons.add(new Person("Agent", "Smith", 1999, 12));
        Person p = persons.get(0);

        PersonStorageWithValue<Worker> workers = new PersonStorageWithValue<>();
        workers.add(new Worker("John", "Doe", 2003, 34));
        Worker w = workers.get(0);

        PersonStorageWithValue<Customer> customers = new PersonStorageWithValue<>();
        customers.add(new Customer("Jane", "Doe", 2004, 56));
        Customer c = customers.get(0);
    }
}
