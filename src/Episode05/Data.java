package Episode05;

public class Data {
    private int data;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Data(int data) {
        this.data = data;
    }

    public int getNegativeData(){
        return -data;
    }

    @Override
    public String toString() {
        return "Data{" +
                "data=" + data +
                '}';
    }

    public static int comparator(Data op1, Data op2) {
        return  op1.getData() < op2.getData() ? -1 : (op1.getData() == op2.getData() ? 0 : 1);
    }
}
