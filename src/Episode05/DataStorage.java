package Episode05;

import java.util.ArrayList;
import java.util.Comparator;

// Extending functionality of ArrayList with
//   - sum
//   - predefined sorts
public class DataStorage<T extends Data> {
    protected final ArrayList<T> items = new ArrayList<>();

    public void add(T newItem){
        items.add(newItem);
    }

    public T get(int index){
        return items.get(index);
    }

    public int size() {
        return items.size();
    }

    //New functionality
    public int sum() {
        int total = 0;
        for (T item : items) {
            total += item.getData();
        }
        return total;
    }

    public void sort1() {
        items.sort(Data::comparator);
    }

    public void sort2() {
        items.sort(
                Comparator.comparingInt(Data::getData)
        );
    }

    public void sortReversed() {
        items.sort(
                (op1, op2) -> Integer.compare(op2.getData(), op1.getData())
        );
    }
}
