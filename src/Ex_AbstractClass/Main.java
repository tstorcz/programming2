package Ex_AbstractClass;

public class Main {

    public class ClassA {

    }

    abstract public class ClassB extends ClassA {
    }

    public class ClassC extends ClassB {

    }

    public void main() {
        ClassA a = new ClassA();
        //ClassB b = new ClassB();
        ClassC c = new ClassC();
        Object o1 = a;
        Object o2 = c;
    }

    class Storage<T extends ClassA> {
        public T item;
    }

    public <T extends ClassA> T test(Storage<T> storage) {
        T newItem = storage.item;
        return newItem;
    }
}
