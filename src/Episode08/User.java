package Episode08;

import java.util.ArrayList;

public class User implements Comparable<User> {
    private String firstName;
    private String lastName;
    private ArrayList<String> roles;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean checkRole(String roleToCheck) {
        for(String r: roles) {
            if(r.equals(roleToCheck))return true;
        }
        return false;
    }

    public User(String firstName, String lastName,
                ArrayList<String> roles) {
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.roles = new ArrayList<>();
        for (String s: roles) {
            this.roles.add(s.trim());
        }
    }

    @Override
    public String toString() {
        return String.format("%s, %s", lastName, firstName);
    }

    @Override
    public int compareTo(User o) {
        if(lastName.compareTo(o.getLastName()) == 0) {
            return firstName.compareTo(o.getFirstName());
        }

        return lastName.compareTo(o.getLastName());
    }
}
