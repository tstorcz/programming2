package Episode08;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class UserLoader {

    public static ArrayList<User> loadFromTextFile(String fileName) throws FileNotFoundException {
        ArrayList<User> users = new ArrayList<>();

        Scanner fileScanner = new Scanner(new File(fileName));

        String userText;
        while(fileScanner.hasNextLine()) {
            userText = fileScanner.nextLine();

            users.add(
                    UserFactory.createUser(userText)
            );
        }

        fileScanner.close();

        return users;
    }

    public static ArrayList<User> loadFromJsonFile(String fileName) throws IOException {
        Type listType = new TypeToken<ArrayList<User>>(){}.getType();

        String jsonString = new String (
                Files.readAllBytes( Paths.get(fileName) )
        );

        Gson gson = new Gson();

        return gson.fromJson(jsonString, listType);
    }
}
