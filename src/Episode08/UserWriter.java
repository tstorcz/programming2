package Episode08;

import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class UserWriter {

    public static String writeJsonFile(String fileName, ArrayList<User> users) throws IOException {
        Gson gson = new Gson();
        String jsonString = gson.toJson(users);

        FileWriter writer = new FileWriter(fileName);
        writer.write(jsonString);
        writer.close();

        return jsonString;
    }

}
