package Episode08;

import java.util.ArrayList;

public class UserFactory {
    public static User createUser(String line) {
        String[] userParts = line.split(",");

        ArrayList<String> roles = new ArrayList<>();
        for(int i=2; i < userParts.length; i++)
            roles.add(userParts[i]);

        return new User(userParts[0], userParts[1], roles);
    }
}
