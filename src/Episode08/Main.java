package Episode08;

// Requires a dependency to be downloaded
//      name: gson-2.8.2.jar (newer version could also work)
//      source: https://github.com/google/gson
//          Look for: Gson .JAR downloads are available from Maven Central.
//          Select version, then download
//      setup: https://www.jetbrains.com/help/idea/working-with-module-dependencies.html#add-a-new-dependency
//          copy downloaded .JAR file to Episode08 folder

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public void main() {

        ArrayList<User> users;

        try {
            users = UserLoader.loadFromTextFile("Data\\Ep08\\Ep08_Users.csv");

            System.out.println("-----Original-----");
            printUserList(users);

            Collections.sort(users);

            System.out.println("-----Sort by lastname and firstname-----");
            printUserList(users);

            System.out.println("-----JSON serialization-----");
            String jsonString = UserWriter.writeJsonFile("Data\\Ep08_Users.json", users);
            System.out.println(jsonString);

            System.out.println("-----JSON deserialize 1-----");
            ArrayList<User> usersCopy;
            Gson gson = new Gson();

            //inaccurate parametrized type specification
            usersCopy = gson.fromJson(jsonString, users.getClass());

            printUserList(usersCopy);

            System.out.println("-----JSON deserialize 2-----");
            usersCopy = UserLoader.loadFromJsonFile("Data\\Ep08_Users.json");
            printUserList(usersCopy);

            System.out.println("-----Role check-----");
            checkRoles(users);

        }catch(IOException ex) {
            System.out.printf("IOException caught in convert!%n%s%n", ex.getMessage());
        }
    }

    private void printUserList(ArrayList<User> users) {
        for(int i=0; i<users.size(); i++) {
            System.out.println(
                    (i+1) + ". " + users.get(i)
            );
        }
    }

    private void checkRoles(ArrayList<User> users) {
        int selectedIndex = 1;

        do {
            Scanner inputScanner = new Scanner(System.in);

            selectedIndex = -1;
            while(selectedIndex<0 || selectedIndex >= users.size()) {
                System.out.println("Please select a user (0-exit): ");

                try {
                    selectedIndex = Integer.parseInt(inputScanner.nextLine()) - 1;
                } catch (NumberFormatException e) {
                    selectedIndex = -1;
                }
            }

            if (selectedIndex >= 0) {
                User selectedUser = users.get(selectedIndex);

                System.out.println("Please select an operation (c/r/u/d): ");
                String selectedOperation = inputScanner.nextLine();

                if (selectedUser.checkRole(selectedOperation)) {
                    System.out.printf("%s has the %s role%n", selectedUser, selectedOperation);
                } else {
                    System.out.printf("%s does NOT have the %s role%n", selectedUser, selectedOperation);
                }
            }
        } while (selectedIndex >= 0);
    }
}
