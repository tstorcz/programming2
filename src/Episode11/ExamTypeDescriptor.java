package Episode11;

public class ExamTypeDescriptor {
    private String name;
    private String dataFile;
    private ExamTypeEvaluator evaluator;

    public String getName() {
        return name;
    }

    public String getDataFile() {
        return dataFile;
    }

    public ExamTypeEvaluator getEvaluator() {
        return evaluator;
    }

    public ExamTypeDescriptor(String name, String dataFile, ExamTypeEvaluator evaluator) {
        this.name = name;
        this.dataFile = dataFile;
        this.evaluator = evaluator;
    }
}
