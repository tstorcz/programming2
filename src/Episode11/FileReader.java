package Episode11;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileReader {
    public static ArrayList<Result> readFile(String fileName) throws IOException {
        ArrayList<Result> results = new ArrayList<>();

        Scanner fs = new Scanner(new File(fileName));
        while(fs.hasNextLine()) {
            results.add(ResultFactory.createFromWords(fs.nextLine().split(";")));
        }
        fs.close();
        return results;
    }
}
