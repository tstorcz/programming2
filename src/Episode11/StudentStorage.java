package Episode11;

import java.util.ArrayList;
import java.util.Collections;

public class StudentStorage {
    private final ArrayList<Student> students = new ArrayList<>();
    private final int examTypeCount;
    private final int examCount;
    private final int maxRetries;

    public int getStudentCount() { return students.size(); }

    public Student getStudent(int index) { return students.get(index); }

    public StudentStorage(int examTypeCount, int examCount, int maxRetries) {
        this.examTypeCount = examTypeCount;
        this.examCount = examCount;
        this.maxRetries = maxRetries;
    }

    public void addResult(int examTypeIndex, int index, Result newResult) {
        Student found = null;
        for(Student s : students) {
            if(s.getId().equalsIgnoreCase(newResult.getStudentId())) {
                found = s;
            }
        }
        if(found == null) {
            found = new Student(newResult.getStudentId(), examTypeCount, examCount, maxRetries);
            students.add(found);
        }

        found.addResult(examTypeIndex, index, newResult.getResult());
    }

    public void sort() {
        Collections.sort(students);
    }

    public int countFailed(ExamTypeEvaluator finalEvaluator, ExamTypeDescriptor[] examTypeDescriptors) {
        int count = 0;
        for(Student s : students) {
            if(s.evaluate(finalEvaluator, examTypeDescriptors) < 0.5) {
                count += 1;
            }
        }
        return count;
    }
}
