package Episode11;

public class WeightedAverageEvaluator implements ExamTypeEvaluator {
    private final double[] weights;

    public WeightedAverageEvaluator(double[] weights) {
        this.weights = weights;
    }

    @Override
    public boolean canEvaluate(int resultCount) {
        return weights.length == resultCount;
    }

    @Override
    public double evaluate(double[] results) {
        if(results.length != weights.length) {
            throw new IllegalArgumentException("Result and weight counts do not match!");
        }

        double sum = 0.0;
        double weightSum = 0.0;

        for(int i = 0; i< results.length; i++) {
            sum += weights[i] * results[i];
            weightSum += weights[i];
        }

        return sum / weightSum;
    }
}
