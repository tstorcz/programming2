package Episode11;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Controller {
    private StudentStorage studentStorage;

    public void process(CourseDescriptor parameters) {
        studentStorage = new StudentStorage(parameters.getExamTypeCount(), parameters.getExamCount(), parameters.getMaxRetryCount());

        for(int examIndex=0; examIndex<parameters.getExamCount(); examIndex++) {
            for(int examTypeIndex=0; examTypeIndex< parameters.getExamTypeCount(); examTypeIndex++) {
                try {
                    for(int tryIndex=0; tryIndex < parameters.getMaxRetryCount() + 1; tryIndex++) {
                        processFile(
                                examTypeIndex, examIndex,
                                parameters.getExamTypeDataFileName(examTypeIndex, examIndex, tryIndex)
                        );
                    }
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }

        studentStorage.sort();

        System.out.printf("Hallgatószám: %d%n", studentStorage.getStudentCount());

        System.out.printf(
                "Sikertelen: %d%n",
                studentStorage.countFailed(parameters.getFinalEvaluator(), parameters.getExamTypedescriptors())
        );

        Student s;
        for(int i=0; i<studentStorage.getStudentCount(); i++) {
            s = studentStorage.getStudent(i);
            System.out.print(s);
            printMark(Mark.get(s.evaluate(parameters.getFinalEvaluator(), parameters.getExamTypedescriptors())));
            for(int j=0; j<parameters.getExamTypeCount();j++) {
                printRequiredResult(" " + parameters.getExamTypeName(j) + " ", s.evaluateExamType(j, parameters.getExamTypeEvaluator(j)));
            }
            System.out.println();
        }
    }

    private void processFile(int examTypeIndex, int index, String fileName) throws IOException {
        ArrayList<Result> results;
        results = FileReader.readFile(fileName);

        for(Result r : results) {
            studentStorage.addResult(examTypeIndex, index, r);
        }
    }

    private void printMark(int mark) {
        if (mark > 1) {
            System.out.print(" Osztályzat: ");
            System.out.print(mark);
        }
    }

    private void printRequiredResult(String examTypeName, double currentResult) {
        if (currentResult < 0.5) {
            System.out.print(" ");
            System.out.print(examTypeName);
            System.out.print(": ");
            System.out.printf("%.2f", (0.5 - 0.4 * currentResult) / 0.6);
        }
    }
}
