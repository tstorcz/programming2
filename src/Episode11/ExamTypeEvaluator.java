package Episode11;

public interface ExamTypeEvaluator {
    boolean canEvaluate(int resultCount);

    double evaluate(double[] results);
}
