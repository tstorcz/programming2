package Episode11;

public class Mark {
    private static final double[] BOUNDS = new double[] {0.5, 0.65, 0.81, 0.91};

    public static int get(double percent) {
        for(int i = 0; i<BOUNDS.length; i++)
            if(percent < BOUNDS[i])return i + 1;
        return 5;
    }
}
