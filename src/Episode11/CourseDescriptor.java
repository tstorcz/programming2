package Episode11;

import java.nio.file.Paths;

public class CourseDescriptor {
    private final ExamTypeDescriptor[] examTypeDescriptors;
    private final ExamTypeEvaluator finalEvaluator;
    private final int examCount;
    private final int maxRetryCount;

    private final String dataFilePath;

    public int getExamTypeCount() {
        return examTypeDescriptors.length;
    }

    public ExamTypeDescriptor[] getExamTypedescriptors() {
        return examTypeDescriptors;
    }

    public String getExamTypeName(int index) { return examTypeDescriptors[index].getName(); }
    public String getExamTypeDataFileName(int typeIndex, int examIndex, int tryIndex) {
        return Paths.get(
                dataFilePath,
                examTypeDescriptors[typeIndex].getDataFile().replace("{exam}", String.valueOf(examIndex+1)).replace("{try}", String.valueOf(tryIndex+1))
        ).toString();
    }

    public ExamTypeEvaluator getExamTypeEvaluator(int index) {
        return examTypeDescriptors[index].getEvaluator();
    }

    public ExamTypeEvaluator getFinalEvaluator() {
        return finalEvaluator;
    }

    public int getExamCount() {
        return examCount;
    }

    public int getMaxRetryCount() {
        return maxRetryCount;
    }

    public String getDataFilePath() {
        return dataFilePath;
    }

    public CourseDescriptor(
            ExamTypeDescriptor[] examTypes,
            int examCount, int maxRetryCount,
            ExamTypeEvaluator finalEvaluator,
            String dataFilePath
    ) {
        this.examTypeDescriptors = examTypes;
        this.examCount = examCount;
        this.maxRetryCount = maxRetryCount;
        this.finalEvaluator = finalEvaluator;
        this.dataFilePath = dataFilePath;
    }
}
