package Episode11;

public class ExamType {
    private final double[] results;
    private final int[] retries;
    private final int maxTryCount;

    public double[] getResults() { return results; }

    public ExamType(int examCount, int maxTryCount) {
        results = new double[examCount];
        retries = new int[examCount];
        this.maxTryCount = maxTryCount;
    }

    public void addResult(int index, Double newResult) {
        if(retries[index] < maxTryCount) {
            results[index] = newResult;
            retries[index]++;
        }
        else {
            throw new IllegalArgumentException(String.format("%d:%d/%d", index, retries[index], maxTryCount));
        }
    }
}
