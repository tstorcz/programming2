package Episode11;

public class Result {
    private String studentId;
    private Double result;

    public Result(String studentId, Double result) {
        this.studentId = studentId;
        this.result = result;
    }

    public String getStudentId() {
        return studentId;
    }

    public Double getResult() { return result; }
}
