package Episode11;

public class ResultFactory {
    public static Result createFromWords(String[] words) {
        return new Result(words[0], Double.parseDouble(words[1].replace(",", ".")));
    }
}
