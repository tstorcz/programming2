package Episode11;

public class Student implements Comparable<Student> {
    private final String id;
    private final ExamType[] examTypes;

    public String getId() { return id; }


    public Student(String id, int examTypeCount, int examCount, int maxRetries) {
        this.id = id;
        examTypes = new ExamType[examTypeCount];
        for(int i=0; i<examTypes.length; i++) {
            examTypes[i] = new ExamType(examCount, maxRetries + 1);
        }
    }

    @Override
    public int compareTo(Student o) {
        return id.compareTo(o.getId());
    }

    public void addResult(int examTypeIndex, int index, Double newResult) {
        examTypes[examTypeIndex].addResult(index, newResult);
    }

    public String toString() {
        return id;
    }

    public Double evaluateExamType(int examTypeIndex, ExamTypeEvaluator theoryEvaluator) {
        return theoryEvaluator.evaluate(examTypes[examTypeIndex].getResults());
    }

    public double evaluate(ExamTypeEvaluator finalEvaluator, ExamTypeDescriptor[] examTypeDescriptors) {
        double results[] = new double[examTypes.length];
        boolean failed = false;

        for(int i=0; i<examTypes.length; i++) {
            results[i] = examTypeDescriptors[i].getEvaluator().evaluate(examTypes[i].getResults());
            if(results[i] < 0.5)
                failed = true;
        }
        return failed ? 0 : finalEvaluator.evaluate(results);
    }
}
