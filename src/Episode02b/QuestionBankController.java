package Episode02b;

import java.io.FileNotFoundException;

public class QuestionBankController {
    public void main() throws FileNotFoundException {
        Question[] questionList = QuestionBankLoader.LoadQuestions("Data\\Ep02\\Ep02_questions.txt");
        QuestionBank bank = new QuestionBank(questionList.length);
        for (Question q : questionList) {
            bank.addQuestion(q);
        }
    }
}
