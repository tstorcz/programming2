package Episode02b;

public class Question {
    private String text;
    private String[] answers;
    private String correct;

    public String getText() {
        return text;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String getCorrect() {
        return correct;
    }

    public Question(String text, String[] answers, String correct) {
        this.text = text;
        this.answers = answers;
        this.correct = correct;
    }
}
