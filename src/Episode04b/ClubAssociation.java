package Episode04b;

public class ClubAssociation {
    private Club clubA;
    private Club clubB;

    public Club getClubA() {
        return clubA;
    }

    public Club getClubB() {
        return clubB;
    }

    public ClubAssociation(Club clubA, Club clubB) {
        this.clubA = clubA;
        this.clubB = clubB;
    }
}
