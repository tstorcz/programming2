package Episode04b;

public class Discipline {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Discipline(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Discipline(String name) {
        this(-1, name);
    }
}
