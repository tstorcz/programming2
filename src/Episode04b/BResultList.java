package Episode04b;

import java.util.ArrayList;

public class BResultList {
    private String name;
    private ArrayList<BResult> results;

    public String getName() { return name; }
    public int getSize() { return results.size(); }
    public BResult getResult(int index) { return results.get(index); }

    public BResultList(String name) {
        this.name = name;
        results = new ArrayList<>();
    }

    public void addResult(BResult newResult) {
        results.add(newResult);
    }
}
