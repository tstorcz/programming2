package Episode04b;

public class Competitor {
    private static String nameFormat = "l f";

    public static String getNameFormat() {
        return nameFormat;
    }

    public static void setNameFormat(String nameFormat) {
        Competitor.nameFormat = nameFormat;
    }

    private int id;
    private String firstName;
    private String lastName;
    private int birthYear;
    private Club club;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getName() {
        switch (nameFormat) {
            case "l f":
                return String.format("%s %s", lastName, firstName);
            case "l, f":
                return String.format("%s, %s", lastName, firstName);
            default:
                return String.format("%s %s", firstName, lastName);
        }
    }

    public Club getClub() {
        return club;
    }

    public Competitor(int id, String name, int birthYear, Club club) {
        this.id = id;
        this.firstName = name;
        this.lastName = "";
        this.birthYear = birthYear;
        this.club = club;
    }

    public Competitor(int id, String firstName, String lastName, Club club) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = -1;
        this.club = club;

        if(firstName.contains("(")) {
            this.birthYear = Integer.parseInt(
                    firstName.substring(firstName.indexOf("(") + 1, firstName.indexOf(")"))
            );
            this.firstName = firstName.substring(0, firstName.indexOf("(")).trim();
        }
    }
}
