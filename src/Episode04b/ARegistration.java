package Episode04b;

public class ARegistration {
    private int competitorId;
    private String competitorName;
    private int competitorBirthYear;

    private int clubId;
    private String clubName;

    private int contestId;
    private int disciplineTypeId;
    private String disciplineTypeName;
    private int disciplineId;
    private String disciplineName;
    private int classId;
    private String className;

    public int getContestId() {
        return contestId;
    }

    public int getDisciplineId() {
        return disciplineId;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public int getDisciplineTypeId() {
        return disciplineTypeId;
    }

    public String getDisciplineTypeName() {
        return disciplineTypeName;
    }

    public int getCompetitorId() {
        return competitorId;
    }

    public String getCompetitorName() {
        return competitorName;
    }

    public int getCompetitorBirthYear() {
        return competitorBirthYear;
    }

    public int getClassId() {
        return classId;
    }

    public String getClassName() {
        return className;
    }

    public int getClubId() {
        return clubId;
    }

    public String getClubName() {
        return clubName;
    }

    public ARegistration(int contestId, int disciplineId, String disciplineName, int disciplineTypeId, String disciplineTypeName, int competitorId, String competitorName, int competitorBirthYear, int classId, String className, int clubId, String clubName) {
        this.contestId = contestId;
        this.disciplineId = disciplineId;
        this.disciplineName = disciplineName;
        this.disciplineTypeId = disciplineTypeId;
        this.disciplineTypeName = disciplineTypeName;
        this.competitorId = competitorId;
        this.competitorName = competitorName;
        this.competitorBirthYear = competitorBirthYear;
        this.classId = classId;
        this.className = className;
        this.clubId = clubId;
        this.clubName = clubName;
    }
}
