package Episode04b;

public class Class {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Class(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Class(String name) {
        this(-1, name);
    }
}
