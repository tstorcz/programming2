package Episode04b;

import java.util.ArrayList;

public class CompetitorDictionary {
    private ArrayList<Competitor> competitors = new ArrayList<>();

    public int getSize() { return competitors.size(); }

    public Competitor getCompetitor(int index) { return competitors.get(index); }

    public Competitor find(Competitor needle) {
        for (Competitor c : competitors) {
            if(c.getId() == needle.getId() ||
                    (c.getId() < 0 && c.getName().equalsIgnoreCase(needle.getName())
                            && c.getBirthYear() == needle.getBirthYear()
                            && c.getClub().getName().equalsIgnoreCase(needle.getClub().getName())
                    )
            ) return c;
        }

        return null;
    }

    public void addCompetitor(Competitor newCompetitor) {
        if(find(newCompetitor) == null) {
            competitors.add(newCompetitor);
        }
    }
}
