package Episode04b;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ResultConverter {
    public void main(String dataPath) {
        ArrayList<ARegistration> registrations = new ArrayList<>();
        ArrayList<BResultList> results = new ArrayList<>();

        File[] files = new File(dataPath).listFiles();
        for (File f : files) {
            if(f.isFile()) {
                if(f.getName().startsWith("Ep04b_A_")) {
                    registrations = loadRegistrations(f.getAbsolutePath());
                }
                if(f.getName().startsWith("Ep04b_B_")) {
                    results.add(
                            BResultListLoader.load(
                                    f.getName().substring(8, f.getName().indexOf(".")),
                                    f.getAbsolutePath()
                            )
                    );
                }
            }
        }

        ClubDictionary aClubs = new ClubDictionary();
        CompetitorDictionary aCompetitors = new CompetitorDictionary();
        for (ARegistration r : registrations) {
            Club club = aClubs.addClub(
                    new Club(r.getClubId(), r.getClubName())
            );
            aCompetitors.addCompetitor(
                    new Competitor(r.getCompetitorId(), r.getCompetitorName(), r.getCompetitorBirthYear(), club)
            );
        }
    }

    //1. Load registrations made in System A (RegistrationA)
    private ArrayList<ARegistration> loadRegistrations(String fileName) {
        File registrationFile = new File(fileName);
        ArrayList<ARegistration> registrations = new ArrayList<>();

        try {
            Scanner registrationReader = new Scanner(registrationFile);
            while (registrationReader.hasNextLine()) {
                String line = registrationReader.nextLine();
                try {
                    registrations.add(ARegistrationParser.parse(line));
                }
                catch (NumberFormatException ex) {
                    System.out.printf("Could not parse registration line: %s%n", line);
                }
            }
            registrationReader.close();
        } catch (FileNotFoundException e) {
            System.out.printf("Could not open registration file: %s%n", fileName);
        }

        return registrations;
    }

    //2. Load results saved in System B (ResultB)
    //    Solved by BResultListLoader.load(...)

    //3. Create catalogs (club, competitor, discipline, class)

    //4.a. Load previously saved matches if there are any (saved in #7)
    //4.b. Find matches in catalogs automatically (by id or name)

    //5. Assign registrations with results based on catalog matches

    //6. Create a result set compatible with System A (based on catalogs of A) -> ResultA

    //7. Store catalog matches to make next matching easier
}
