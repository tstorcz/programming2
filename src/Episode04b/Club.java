package Episode04b;

public class Club {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Club(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Club(String name) {
        this(-1, name);
    }
}
