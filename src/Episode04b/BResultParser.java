package Episode04b;

public class BResultParser {
    public static BResult parse(String line) {
        String[] values = line.split(";");

        double[] series = new double[values.length - 12];
        boolean isDecimal = false;

        for(int i = 0; i < series.length; i++) {
            isDecimal = isDecimal || values[10 + i].contains(".");
            series[i] = Double.parseDouble(values[10 + i]);
        }

        return new BResult(
                Integer.parseInt(values[0]),
                Integer.parseInt(values[1]),
                Integer.parseInt(values[2]),
                values[3], values[4],
                values[5], values[6],
                values[7], values[8],
                values[9],
                series, Double.parseDouble(values[values.length - 2]), isDecimal,
                values[values.length - 1]
        );
    }
}
