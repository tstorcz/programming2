# Case study
  
The task is to merge data from 2 different sources and create filtered subsets.
  
Data sources:
- registration system: contains registrations of competitors for disciplines  
- result system: contains results of competitors performed in disciplines
  
### Data:  
- registration file: ```Ep04b_A_nevezettek_6531.csv```
  - Competition id
  - Discipline id
  - Discipline name
  - Discipline type id
  - Discipline type name
  - Competitor id
  - Competitor name
  - Birth year
  - Age group id
  - Age group name
  - Club id
  - Club name
- result list file: ```Ep04b_B_<discipline_name>.csv```
  - Rank
  - StartNo
  - ShooterID
  - LastName
  - FirstName
  - Country
  - Discipline
  - Class
  - Club
  - Team
  - Series (2-6)
  - Total
  - Remarks
  
### Notes:
- a competitor could be registered for many disciplines
- many competitors could be registered for a discipline
- names could be mistyped or could not contain accents of hungarian letters
- names could contain last 2 digits of birth year "Kiss Ivett (98)"
- names are in hingarian format (Lastname Firstname)
  
## Task 
Tasks to solve:
- match registrations with results automatically based on names
- accents, startsWith