package Episode04b;

import java.util.ArrayList;

public class ClubDictionary {
    private ArrayList<Club> clubs = new ArrayList<>();

    public int getSize() { return clubs.size(); }

    public Club getClub(int index) { return clubs.get(index); }

    public Club find(Club needle) {
        for (Club c : clubs) {
            if(c.getId() == needle.getId() ||
                    (c.getId() < 0 && c.getName().equalsIgnoreCase(needle.getName()))
            ) return c;
        }

        return null;
    }

    public Club addClub(Club newClub) {
        Club found = find(newClub);
        if(found == null) {
            clubs.add(newClub);
            return newClub;
        }
        else {
            return found;
        }
    }
}
