package Episode09.Controllers;

import Episode09.Models.Business.VehicleStorage;
import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.util.Scanner;

public class StorageController extends BaseController {

    public StorageController(Scanner scanner) {
        super(scanner);
    }

    public VehicleStorage registrationOfSite() throws IOException {
        SingletonNetworkLogger.getSharedInstance().write("Site registration", 4);

        return new VehicleStorage(
                getNumberFromConsole(
                        "Number of parking places",
                        1, 100
                )
        );
    }

}
