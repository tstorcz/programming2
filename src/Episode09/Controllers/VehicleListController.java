package Episode09.Controllers;

import Episode09.Models.Business.Vehicle;
import Episode09.Models.Business.VehicleStorage;

import java.util.Scanner;

public class VehicleListController extends BaseController {
    private VehicleStorage storage;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public VehicleListController(Scanner scanner, VehicleStorage storage) {
        super(scanner);
        this.storage = storage;
        title = "";
    }

    public void printVehicleList() {
        System.out.println(title);
        for(int i = 0; i < storage.getVehicleCount(); i++) {
            System.out.println(storage.getVehicle(i));
        }
    }
}
