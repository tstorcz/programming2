package Episode09.Controllers;

import Episode09.Factories.BusFactory;
import Episode09.Factories.CarFactory;
import Episode09.Factories.VehicleFactory;
import Episode09.Models.Business.VehicleStorage;
import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.util.Scanner;

public class VehicleRegistrationController extends BaseController {
    private final VehicleStorage storage;

    public VehicleRegistrationController(Scanner scanner, VehicleStorage storage) {
        super(scanner);
        this.storage = storage;
    }

    public void registrationOfVehicles() throws IOException {
        boolean wantMore;

        SingletonNetworkLogger.getSharedInstance().write(
                "Registrations of vehicles",
                4
        );

//        CarController carController = new CarController(scanner);
//        BusController busController = new BusController(scanner);
        VehicleController vehicleController = new VehicleController(scanner);
        String answer;
        do {
            System.out.println("What to register? (B-bus/C-car)");
            answer = scanner.nextLine();

            // instead of using dependent controller class for item creation
//            if(answer.equalsIgnoreCase("c")) {
//                storage.register(carController.getVehicle());
//            }
//            else {
//                storage.register(busController.getVehicle());
//            }

            //Select factory to inject
            VehicleFactory selectedFactory;
            if(answer.equalsIgnoreCase("c")) {
                selectedFactory = new CarFactory();
            }
            else {
                selectedFactory = new BusFactory();
            }

            //Inject independent (factory) class to vehicle creator
            storage.register(
                vehicleController.createVehicle(
                        selectedFactory
                )
            );

            System.out.println("Want more? (y/n)");
            answer = scanner.nextLine();
            wantMore = answer.equalsIgnoreCase("y");

        } while (wantMore);
    }
}
