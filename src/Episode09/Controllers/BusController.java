package Episode09.Controllers;

import Episode09.Factories.BusFactory;
import Episode09.Models.Business.Vehicle;

import java.io.IOException;
import java.util.Scanner;

public class BusController extends BaseController {
    public BusController(Scanner scanner) {
        super(scanner);
    }

    public Vehicle getVehicle() throws IOException {
        String registrationNumber;
        String makeModel;
        int creationDate;
        int numberOfSeats;

        System.out.println("Registration number: ");
        registrationNumber = scanner.nextLine();

        System.out.println("Make and model: ");
        makeModel = scanner.nextLine();

        creationDate = getNumberFromConsole(
                "Creation year: ",
                1950, Vehicle.CREATION_YEAR_MAX
        );

        numberOfSeats = getNumberFromConsole(
                "Number of seats: ",
                1, 200
        );

        return new BusFactory().createVehicle(
                registrationNumber, makeModel,
                creationDate, numberOfSeats
        );
    }
}
