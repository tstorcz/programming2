package Episode09.Controllers;

import Episode09.Models.Business.Vehicle;
import Episode09.Models.Business.VehicleStorage;
import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.util.Scanner;

public class MainController extends BaseController {
    private VehicleStorage storage;

    public MainController(Scanner scanner) {
        super(scanner);
    }

    public void start() throws IOException {

        storage = new StorageController(scanner).registrationOfSite();
        SingletonNetworkLogger.getSharedInstance().write("Site created", 3);

        new VehicleRegistrationController(scanner, storage).registrationOfVehicles();
        SingletonNetworkLogger.getSharedInstance().write("Vehicles registered", 3);

        VehicleListController vehicleListController = new VehicleListController(scanner, storage);
        vehicleListController.setTitle("Original list");
        vehicleListController.printVehicleList();
        SingletonNetworkLogger.getSharedInstance().write("Vehicles listed", 5);

        storage.sort();
        SingletonNetworkLogger.getSharedInstance().write("Vehicles sorted", 5);

        vehicleListController.setTitle("Sorted list");
        vehicleListController.printVehicleList();
        SingletonNetworkLogger.getSharedInstance().write("Vehicles listed after sort", 5);

        System.out.print("Find registration number: ");
        String regNumToFind = scanner.nextLine();

        Vehicle found = storage.find(regNumToFind);
        if(found != null) {
            SingletonNetworkLogger.getSharedInstance().write("Vehicle found to modify", 3);
            System.out.println(found);
            new VehicleController(scanner).modifyVehicle(found);
        }
        else {
            SingletonNetworkLogger.getSharedInstance().write("Vehicle not found", 3);
            System.out.println("Vehicle not found");
        }

        vehicleListController.setTitle("Modified list");
        vehicleListController.printVehicleList();
        SingletonNetworkLogger.getSharedInstance().write("Vehicles listed after modification", 4);
    }
}
