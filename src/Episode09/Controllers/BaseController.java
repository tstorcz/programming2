package Episode09.Controllers;

import Episode09.Models.Business.Vehicle;
import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.util.Scanner;

public abstract class BaseController {
    protected Scanner scanner;

    public BaseController(Scanner scanner) {
        this.scanner = scanner;
    }

    protected int getNumberFromConsole(String message, int min, int max) throws IOException
    {
        return getNumberFromConsole(message, min, max, false, 0);
    }

    protected int getNumberFromConsole(
            String message,
            int min,
            int max,
            boolean acceptEmpty,
            int defaultValue
    ) throws IOException {
        String entered;
        boolean hasError = false;
        int number = defaultValue;
        do {
            System.out.printf("%s (%d-%d)%n", message, min, max);
            try {
                entered = scanner.nextLine();
                if(acceptEmpty && entered.isEmpty()){
                    return defaultValue;
                }
                number = Integer.parseInt(entered);
            } catch (NumberFormatException e) {
                hasError = true;
                System.out.println("Please enter an integer");
                SingletonNetworkLogger.getSharedInstance().write(
                        "Not entered an integer",
                        1
                );
            }
        }while (number < min || number > max || hasError);

        return number;
    }
}
