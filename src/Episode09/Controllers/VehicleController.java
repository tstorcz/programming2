package Episode09.Controllers;

import Episode09.Models.Business.Vehicle;
import Episode09.Factories.VehicleFactory;

import java.io.IOException;
import java.util.Scanner;

public class VehicleController extends BaseController {
    public VehicleController(Scanner scanner) {
        super(scanner);
    }

    public Vehicle createVehicle(VehicleFactory factory) throws IOException {
        String registrationNumber;
        String makeModel;
        int creationDate;
        int numberOfSeats;

        System.out.println("Registration number: ");
        registrationNumber = scanner.nextLine();

        System.out.println("Make and model: ");
        makeModel = scanner.nextLine();

        creationDate = getNumberFromConsole(
                "Creation year: ",
                1950, Vehicle.CREATION_YEAR_MAX
        );

        numberOfSeats = getNumberFromConsole(
                "Number of seats: ",
                1, 200
        );

        return factory.createVehicle(
                registrationNumber, makeModel,
                creationDate, numberOfSeats
        );
    }

    public void modifyVehicle(Vehicle item) throws IOException {
        String registrationNumber;
        String makeModel;

        System.out.printf("Registration number [%s]: %n",
                item.getRegistrationNumber()
        );
        registrationNumber = scanner.nextLine();
        if(!registrationNumber.isEmpty()){
            item.setRegistrationNumber(registrationNumber);
        }

        System.out.printf("Make and model [%s]: %n",
                item.getMakeAndModel()
        );

        makeModel = scanner.nextLine();
        if(!makeModel.isEmpty()){
            item.setMakeAndModel(makeModel);
        }

        item.setCreationYear(
                getNumberFromConsole(
                        String.format("Creation year [%d]: ", item.getCreationYear()),
                        1950, Vehicle.CREATION_YEAR_MAX,
                        true,
                        item.getCreationYear()
                )
        );

        item.setNumberOfSeats(
                getNumberFromConsole(
                        String.format("Number of seats [%d]: ", item.getNumberOfSeats()),
                        1, 200,
                        true,
                        item.getNumberOfSeats()
                )
        );
    }
}
