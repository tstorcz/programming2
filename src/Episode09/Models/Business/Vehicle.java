package Episode09.Models.Business;

public abstract class Vehicle implements Comparable<Vehicle> {
    public static final int CREATION_YEAR_MAX = 2022;

    private String registrationNumber;
    private String makeAndModel;
    private int creationYear;
    private int numberOfSeats;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public int getCreationYear() {
        return creationYear;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public void setCreationYear(int creationYear) {
        this.creationYear = creationYear;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Vehicle(String registrationNumber, String makeAndModel, int creationYear, int numberOfSeats) {
        this.registrationNumber = registrationNumber;
        this.makeAndModel = makeAndModel;
        this.creationYear = creationYear;
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return String.format("%s - %s with seats %d [%d]",
                registrationNumber, makeAndModel, numberOfSeats, creationYear
        );
    }

    @Override
    public int compareTo(Vehicle o) {
        return registrationNumber.compareTo(o.getRegistrationNumber());
    }
}
