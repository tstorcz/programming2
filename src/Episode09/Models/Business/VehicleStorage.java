package Episode09.Models.Business;

import java.util.ArrayList;
import java.util.Collections;

public class VehicleStorage {
    private ArrayList<Vehicle> vehicles;
    public final int maxNumberOfVehicles;

    public int getVehicleCount() {
        return vehicles.size();
    }
    public Vehicle getVehicle(int index) {
        return vehicles.get(index);
    }

    public VehicleStorage(int maxNumberOfVehicles) {
        vehicles = new ArrayList<>();
        this.maxNumberOfVehicles = maxNumberOfVehicles;
    }

    public void register(Vehicle newVehicle) {
        if(vehicles.size() < maxNumberOfVehicles) {
            vehicles.add(newVehicle);
        }
        else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public void sort() {
        Collections.sort(vehicles);
    }

    public Vehicle find(String registrationNumber) {
        //Using Java 8 Stream API

        return vehicles
                .stream()                   //Convert the ArrayList to stream of items
                .filter(v -> v.getRegistrationNumber().equalsIgnoreCase(registrationNumber)) //Filter the stream
                .findAny()                  //Get a result of filter
                .orElse(null);         //Or return null
    }
}
