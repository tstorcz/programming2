package Episode09.Models.Business;

public class Car extends Vehicle {
    public Car(String registrationNumber, String makeAndModel, int creationYear, int numberOfSeats) {
        super(registrationNumber, makeAndModel, creationYear, numberOfSeats);
    }

    @Override
    public String toString() {
        return String.format("Car: %s", super.toString());
    }
}
