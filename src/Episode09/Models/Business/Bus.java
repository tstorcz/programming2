package Episode09.Models.Business;

public class Bus extends Vehicle {
    public Bus(String registrationNumber, String makeAndModel, int creationYear, int numberOfSeats) {
        super(registrationNumber, makeAndModel, creationYear, numberOfSeats);
    }

    @Override
    public String toString() {
        return String.format("Bus: %s", super.toString());
    }
}
