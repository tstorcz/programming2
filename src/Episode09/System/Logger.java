package Episode09.System;

import Episode09.Models.System.LogMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    private int logLevel;
    private ArrayList<IOutputChannel> outputChannels;

    // 1-Fatal, 2-Error, 3-Warning, 4-Info, 5-Verbose
    public int getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }

    public void addChannel(IOutputChannel newChannel) {
        outputChannels.add(newChannel);
    }

    public Logger() {
        this.logLevel = 5;
        this.outputChannels = new ArrayList<>();
    }

    public void write(String message, int messageLogLevel) throws IOException {
        LogMessage newMessage = new LogMessage(
                new Date(),
                messageLogLevel,
                message
        );

        if(messageLogLevel <= logLevel) {
            sendMessage(newMessage);
        }
    }

    protected void sendMessage(LogMessage message) throws IOException {
        for (IOutputChannel channel: outputChannels) {
            channel.sendMessage(message);
        }
    }
}
