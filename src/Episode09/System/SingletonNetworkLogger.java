package Episode09.System;

public class SingletonNetworkLogger extends NetworkLogger {
    private static final SingletonNetworkLogger sharedInstance = new SingletonNetworkLogger();

    public static SingletonNetworkLogger getSharedInstance() {
        return sharedInstance;
    }

    private SingletonNetworkLogger() {
        super();
    }
}
