package Episode09.System;

import Episode09.Models.System.LogMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class NetworkLogger extends Logger {
    private boolean isOnline;
    private ArrayList<LogMessage> messageBuffer;

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) throws IOException {
        isOnline = online;
        if(isOnline) {
            //Send buffered messages to output
            for(LogMessage m: messageBuffer){
                sendMessage(m);
            }
            messageBuffer.clear();
        }
    }

    public NetworkLogger() {
        super();
        this.isOnline = true;
        this.messageBuffer = new ArrayList<>();
    }

    public void write(String message, int messageLogLevel) throws IOException {
        LogMessage newMessage = new LogMessage(
                new Date(),
                messageLogLevel,
                message
        );

        if(messageLogLevel <= getLogLevel()) {
            if(isOnline) {
                //Send message to output if on-line
                sendMessage(newMessage);
            }
            else {
                //Store message in buffer if off-line
                messageBuffer.add(newMessage);
            }
        }
    }
}
