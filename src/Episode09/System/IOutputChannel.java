package Episode09.System;

import Episode09.Models.System.LogMessage;

import java.io.IOException;

public interface IOutputChannel {
    void sendMessage(LogMessage message) throws IOException;
}
