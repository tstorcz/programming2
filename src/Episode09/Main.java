package Episode09;

import Episode09.Controllers.MainController;
import Episode09.System.FileOutputChannel;
import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public void main() {
        try {
            SingletonNetworkLogger
                    .getSharedInstance()
                    .addChannel(
                            new FileOutputChannel("Data\\Ep09\\Ep09_Events.log")
                    );

            SingletonNetworkLogger.getSharedInstance().setLogLevel(5);
            SingletonNetworkLogger.getSharedInstance().write(
                    "App started",
                    4
            );

            new MainController(
                    new Scanner(System.in)
            ).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
