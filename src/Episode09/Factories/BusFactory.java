package Episode09.Factories;

import Episode09.Models.Business.Bus;
import Episode09.Models.Business.Vehicle;

public class BusFactory extends VehicleFactory {
    @Override
    public Vehicle createVehicle(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        return new Bus(registrationNumber, makeModel, creationYear, numberOfSeats);
    }
}
