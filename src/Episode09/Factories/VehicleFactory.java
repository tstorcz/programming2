package Episode09.Factories;

import Episode09.Models.Business.Vehicle;

public abstract class VehicleFactory {
    public abstract Vehicle createVehicle(
            String registrationNumber,
            String makeModel,
            int creationYear,
            int numberOfSeats
    );

}
