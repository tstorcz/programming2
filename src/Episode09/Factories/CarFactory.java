package Episode09.Factories;

import Episode09.Models.Business.Car;
import Episode09.Models.Business.Vehicle;

public class CarFactory extends VehicleFactory {
    @Override
    public Vehicle createVehicle(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        return new Car(registrationNumber, makeModel, creationYear, numberOfSeats);
    }
}
