package Episode04;

public class Child extends Parent {

    public static String getStaticClassName()
    {
        return "StaticChild";
    }

    public String getClassName(String a)
    {
        return "Child " + a;
    }

    public void finalMethod() {
        System.out.println("final method called in Child instance");
    }
}
