package Episode04;

public class Car extends Vehicle {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Car.baseRentalFee = baseRentalFee;
    }

    private int comfortLevel;

    public int getComfortLevel() {
        return comfortLevel;
    }

    public Car(
            String licence, int prodYear, double consumption,
            int comfortLevel
    ) {
        super(licence, prodYear, consumption);
        this.comfortLevel = comfortLevel;
    }

    @Override
    public String toString() {
        return String.format("Car %s comfort level: %d",
                super.toString(), comfortLevel
        );
    }
}
