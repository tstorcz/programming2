package Episode04;

public class Test {
    //Object reference cast to parent can be implicitly done
    //Object reference cast to child may be invalid, so can be done only explicitly
    public static void testInstancePolymorphism() {
        Parent parent = new Parent();
        Child child = new Child();

        //Instance polymorphism
        System.out.println(parent.getClassName("p"));
        System.out.println(child.getClassName("c"));

        Parent parent2 = child;         //implicit cast to parent
        //Child childBad = parent2;     // Wrong implicit cast
        Child child2 = (Child)parent2;  //explicit cast to child

        Child2 c2a = new Child2();
        Parent p3 = c2a;
        Child2 c2b = (Child2) p3;

        //Result of using same object via different interfaces
        System.out.println(parent2.getClassName("a1"));
        System.out.println(child2.getClassName("a2"));

        //When method is not final indeed, call works perfectly
        //Try to disable overriding: set method final in Parent class
        parent.finalMethod();
        child.finalMethod();
    }

    public static void testClassPolymorphism() {
        //Class polymorphism
        System.out.println(Parent.getStaticClassName());
        System.out.println(Child.getStaticClassName());

        Child child = new Child();
        Parent parent2 = child;         //implicit cast to parent
        Child child2 = (Child)parent2;  //explicit cast to child

        //Result of using same class via different interfaces
        //This is a bad practice, use class methods with class name context
        System.out.println(child.getStaticClassName());
        System.out.println(parent2.getStaticClassName());
        System.out.println(child2.getStaticClassName());
    }
}
