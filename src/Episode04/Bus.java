package Episode04;

public class Bus extends Vehicle {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Bus.baseRentalFee = baseRentalFee;
    }

    private int passengers;

    public int getPassengers() {
        return passengers;
    }

    public Bus(String licence, int prodYear, double consumption, int passengers) {
        super(licence, prodYear, consumption);
        this.passengers = passengers;
    }

    public String toString() {
        return String.format("Bus %s number of passengers: %d", super.toString(), passengers);
    }
}
