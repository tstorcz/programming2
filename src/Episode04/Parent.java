package Episode04;

public class Parent {

    public static String getStaticClassName()
    {
        return "StaticParent";
    }

    protected String getClassName(String argument)
    {
        return "Parent " + argument;
    }

    public void finalMethod() {
        System.out.println("final method called");
    }
}
