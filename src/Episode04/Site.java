package Episode04;

import java.util.ArrayList;

public class Site {
    private ArrayList<Vehicle> vehicles;
    private String name;
    private String address;
    private String id;
    private int maxVehicleCount;

    //What happens if a reference of a collection is published?
    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public int getMaxVehicleCount() {
        return maxVehicleCount;
    }

    //=========== Publish the list size and an item getter
    public int getNumberOfVehicles() {
        return vehicles.size();
    }

    public Vehicle getVehicle(int index) {
        return vehicles.get(index);
    }
    //===========

    public Site(String name, String address, String id, int maxVehicleCount) {
        this.name = name;
        this.address = address;
        this.id = id;
        this.maxVehicleCount = maxVehicleCount;
        this.vehicles = new ArrayList<>();
    }

    public void registerVehicle(Vehicle newVehicle) {
        if(vehicles.size() < maxVehicleCount) {
            vehicles.add(newVehicle);
        }
    }

    public Vehicle find(String licence) {
        for(Vehicle v: vehicles) {
            if(v.getLicence().equalsIgnoreCase(licence)) {
                return v;
            }
        }
        return null;
    }

    public Bus findBus(int numberOfPersons) {
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Bus &&
                    ((Bus)v).getPassengers() >= numberOfPersons
            ) {
                return (Bus)v;
            }
        }
        return null;
    }

    public Car findCar(int comfortLevel) {
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Car
                    && ((Car)v).getComfortLevel() >= comfortLevel
            ) {
                return (Car)v;
            }
        }
        return null;
    }

    //Write method to get all cars higher than or equal to a certain comfort level

    //Write method to get all buses which can transport passengers more than or equal to a certain number
}






