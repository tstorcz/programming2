package Episode04;

import java.util.Scanner;

public class Main {
    public void polymorphismTest() {
        //======================== Find the difference between override and hide
        //Polymorphism test
        Test.testInstancePolymorphism();

        Test.testClassPolymorphism();
    }

    public void main() {
        //======================== Example of inheritance
        System.out.println();
        System.out.println();

        //Vehicle rental company example
        Scanner consoleScanner = new Scanner(System.in);

        System.out.print("Name of site: ");
        String name = consoleScanner.nextLine();

        System.out.print("Address of site: ");
        String address = consoleScanner.nextLine();

        System.out.print("ID of site: ");
        String siteId = consoleScanner.nextLine();

        System.out.print("Max number of vehicles: ");
        int maxVehicles = consoleScanner.nextInt();

        Site mySite = new Site(name, address, siteId, maxVehicles);

        mySite.registerVehicle(
                new Car("AAA111", 2002, 7.0, 5)
        );

        mySite.registerVehicle(
                new Bus("BBB111", 2012, 9.7, 25)
        );

        mySite.registerVehicle(
                new Car("AAA222", 2015, 5.0, 3)
        );

        mySite.registerVehicle(
                new Bus("BBB222", 2018, 9.0, 38)
        );

        System.out.println();

        list(mySite);

        System.out.println();

        System.out.println(mySite.findCar(4));
        System.out.println(mySite.findBus(30));
    }

    private void list(Site site) {
        for(Vehicle v: site.getVehicles()) {
            System.out.println(v);
        }
    }
}
