package Ex_ListSort;

import java.util.Random;

public class Main {

    Random rnd = new Random();

    class MyClass implements Comparable<MyClass> {
        private Integer value = rnd.nextInt(100);

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public int compareTo(MyClass o) {
            MyClass other = (MyClass)o;

            //return value < other.value ? -1 : ( value == other.value ? 0 : 1);
            return value.compareTo(other.value);
        }
    }

    class MyClass2 extends MyClass {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public void main() {
        MyClass2 c2 = new MyClass2();

        MyClass c = c2;

        Comparable cmp = c2;
    }
}
