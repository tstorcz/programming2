package Episode08b;

import java.io.FileWriter;
import java.io.IOException;

public class FileLog extends LogChannel {
    private String fileName;

    public FileLog(String id, String fileName) {
        super(id);
        this.fileName = fileName;
    }

    @Override
    public void write(String message) throws IOException {
        FileWriter fw = new FileWriter(this.fileName, true);
        fw.write(message);
        fw.close();
    }
}
