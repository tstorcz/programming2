package Episode08b;

public class ConsoleLog extends LogChannel {
    public ConsoleLog(String id) {
        super(id);
    }

    @Override
    public void write(String message) {
        System.out.println(message);
    }
}
