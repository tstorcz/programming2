package Episode08b;

import java.io.IOException;

public class Main {
    public void start() {
        Logger logger = new Logger();
        logger.setLevel(4);

        logger.addChannel(new ConsoleLog("console"));
        logger.addChannel(new FileLog("file", "Data\\Ep08\\Ep08.log"));

        try {
            logger.write("Starting...", 4);
            logger.write("Starting extra information", 5);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
