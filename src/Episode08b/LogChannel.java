package Episode08b;

import java.io.IOException;

public abstract class LogChannel {
    private final String id;

    public String getId() {
        return id;
    }

    public LogChannel(String id) {
        this.id = id;
    }

    public abstract void write(String message) throws IOException;
}
