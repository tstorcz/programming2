package Episode08b;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    //0-None, 1-Critical, 2-Error, 3-Warning, 4-Info, 5-Verbose
    private int level = 3;
    private final ArrayList<LogChannel> channels = new ArrayList<>();

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        if(level >= 0 && level <= 5) {
            this.level = level;
        }
    }

    public void addChannel(LogChannel newChannel) {
        channels.add(newChannel);
    }

    public void write(String message, int level) throws IOException {
        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        String extendedMessage = String.format("%s [L%d] %s\n",
                formatter.format(new Date()),
                level,
                message
        );

        if(level > 0 && level <= this.level) {
            for (LogChannel c : channels) {
                c.write(extendedMessage);
            }
        }
    }
}
