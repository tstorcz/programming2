package Episode07b;

public class Database {
    private final int competitionId;
    private final DataSource sourceA;
    private final DataSource sourceB;

    private final AssociationList<ComparableNamedComponent> clubAssociations = new AssociationList<>();
    private final AssociationList<Competitor> competitorAssociations = new AssociationList<>();
    private final AssociationList<ComparableNamedComponent> disciplineAssociations = new AssociationList<>();
    private final AssociationList<ComparableNamedComponent> ageGroupAssociations = new AssociationList<>();

    public DataSource getSourceA() {
        return sourceA;
    }

    public DataSource getSourceB() {
        return sourceB;
    }

    public AssociationList<ComparableNamedComponent> getClubAssociations() {
        return clubAssociations;
    }

    public AssociationList<Competitor> getCompetitorAssociations() {
        return competitorAssociations;
    }

    public AssociationList<ComparableNamedComponent> getDisciplineAssociations() {
        return disciplineAssociations;
    }

    public AssociationList<ComparableNamedComponent> getAgeGroupAssociations() {
        return ageGroupAssociations;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public Database(int competitionId, DataSource sourceA, DataSource sourceB) {
        this.competitionId = competitionId;
        this.sourceA = sourceA;
        this.sourceB = sourceB;
    }

    public void findDictionaryMatches() {
        findComparableNamedComponentMatches(sourceA.getClubs(), sourceB.getClubs(), clubAssociations);
        findComparableNamedComponentMatches(sourceA.getDisciplines(), sourceB.getDisciplines(), disciplineAssociations);
        findComparableNamedComponentMatches(sourceA.getAgeGroups(), sourceB.getAgeGroups(), ageGroupAssociations);
        findCompetitorMatches();
    }

    private void findComparableNamedComponentMatches(
            Dictionary<ComparableNamedComponent> a, Dictionary<ComparableNamedComponent> b,
            AssociationList<ComparableNamedComponent> associations
    ) {
        ComparableNamedComponent match;
        for (ComparableNamedComponent c : a) {
            match = b.find(c);
            if(match != null) {
                associations.addItem(new Association<>(c, match));
            }
        }
    }

    private void findCompetitorMatches() {
        Association<ComparableNamedComponent> clubAssociation;
        Competitor match;
        for(Competitor c : sourceA.getCompetitors()) {
            clubAssociation = clubAssociations.findByA(c.getClub());
            match = sourceB.getCompetitors().find(
                    new Competitor(c, clubAssociation == null ? c.getClub() : clubAssociation.getB())
            );
            if(match != null) {
                competitorAssociations.addItem(new Association<>(c, match));
            }
        }
    }

    public void copyResultsFromBToA() {
        Association<Competitor> competitorMatch;
        Association<ComparableNamedComponent> disciplineMatch;
        Association<ComparableNamedComponent> ageGroupMatch;
        Result b;
        for(Result r : sourceA.getResults()){
            competitorMatch = competitorAssociations.findByA(r.getCompetitor());
            disciplineMatch = disciplineAssociations.findByA(r.getDiscipline());
            ageGroupMatch = ageGroupAssociations.findByA(r.getAgeGroup());

            if(competitorMatch != null && disciplineMatch != null && ageGroupMatch != null) {
                b = sourceB.findResult(competitorMatch.getB(), disciplineMatch.getB(), ageGroupMatch.getB());
                if(b != null) {
                    r.copyResults(b);
                    r.addMatch();
                    b.addMatch();
                }
            }
        }
    }
}
