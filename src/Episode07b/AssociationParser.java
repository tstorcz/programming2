package Episode07b;

import java.io.FileWriter;
import java.io.IOException;

public class AssociationParser<T extends Comparable<T>> {
    public void save(
            String fileName, Dictionary<T> dictionaryA, Dictionary<T> dictionaryB,
            AssociationList<T> associationList
    )  {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);
            Association<T> association;
            for (int i = 0; i < dictionaryA.getSize(); i++) {
                association = associationList.findByA(dictionaryA.getItem(i));
                fw.write(
                        dictionaryA.getItem(i).toString() + "; "
                                + (association == null ? ";" : association.getB().toString())
                                + "\n"
                );
            }
            for (int i = 0; i < dictionaryB.getSize(); i++) {
                association = associationList.findByB(dictionaryB.getItem(i));
                if (association == null) {
                    fw.write("; ; " + dictionaryB.getItem(i).toString() + "\n");
                }
            }
            fw.close();
        }
        catch (IOException e) {
            System.out.println("Could not write file: " + fileName);
        }
    }

    public Association<T> parseLine(String line, Dictionary<T> a, Dictionary<T> b, Factory<T> factory) {
        if(line.endsWith(";"))line += " ";
        String[] values = line.split(";");

        String[] aValues = new String[values.length / 2];
        String[] bValues = new String[aValues.length];

        copyStringArray(values, 0, aValues);
        copyStringArray(values, aValues.length, bValues);

        T foundA = null;
        T foundB = null;
        try {
            foundA = a.find(factory.create(aValues));
            foundB = b.find(factory.create(bValues));
        }
        catch (Exception ex) {
            //suppress exception, no association loaded
        }

        if(foundA != null && foundB != null) {
            return new Association<T>(foundA, foundB);
        }

        return null;
    }

    private void copyStringArray(String[] source, int startPos, String[] destination) {
        for(int i = 0; i < destination.length; i++) {
            destination[i] = source[i + startPos].trim();
        }
    }
}
