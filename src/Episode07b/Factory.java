package Episode07b;

public interface Factory<T> {
    T create(String[] values);
}
