package Episode07b;

public class Association<T extends Comparable<T>> implements Comparable<Association<T>> {
    private final T itemA;
    private final T itemB;

    public T getA() {
        return itemA;
    }

    public T getB() {
        return itemB;
    }

    public Association(T itemA, T itemB) {
        this.itemA = itemA;
        this.itemB = itemB;
    }

    @Override
    public int compareTo(Association<T> o) {
        if(itemA.compareTo(o.getA()) == 0)
            return itemB.compareTo(o.getB());
        return itemA.compareTo(o.getA());
    }

    @Override
    public String toString() {
        return itemA + "; " + itemB;
    }
}
