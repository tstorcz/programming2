package Episode07b;

public abstract class NamedComponent {
    private final int id;
    private final String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameWithoutAccent() {
        String name = getName();
        name = name.replace("ö", "o");
        name = name.replace("Ö", "O");
        name = name.replace("ó", "o");
        name = name.replace("Ó", "O");
        name = name.replace("ő", "o");
        name = name.replace("Ő", "O");

        name = name.replace("ü", "u");
        name = name.replace("Ü", "U");
        name = name.replace("ú", "u");
        name = name.replace("Ú", "U");
        name = name.replace("ű", "u");
        name = name.replace("Ű", "U");

        name = name.replace("é", "e");
        name = name.replace("É", "E");

        name = name.replace("á", "a");
        name = name.replace("Á", "A");

        return name;
    }

    public NamedComponent(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public NamedComponent(String name) {
        this.id = -1;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%d; %s", id, name);
    }
}
