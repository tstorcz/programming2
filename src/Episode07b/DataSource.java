package Episode07b;

import java.util.ArrayList;

public abstract class DataSource {
    protected final Dictionary<ComparableNamedComponent> clubs;
    protected final Dictionary<Competitor> competitors;
    protected final Dictionary<ComparableNamedComponent> disciplines;
    protected final Dictionary<ComparableNamedComponent> ageGroups;
    protected final ArrayList<Result> results;

    public Dictionary<ComparableNamedComponent> getClubs() {
        return clubs;
    }

    public Dictionary<Competitor> getCompetitors() {
        return competitors;
    }

    public Dictionary<ComparableNamedComponent> getDisciplines() {
        return disciplines;
    }

    public Dictionary<ComparableNamedComponent> getAgeGroups() {
        return ageGroups;
    }

    public ArrayList<Result> getResults() {
        return results;
    }

    public DataSource() {
        clubs = new Dictionary<>();
        competitors = new Dictionary<>();
        disciplines = new Dictionary<>();
        ageGroups = new Dictionary<>();
        results = new ArrayList<>();
    }

    public Result findResult(
            Competitor competitor,
            ComparableNamedComponent discipline,
            ComparableNamedComponent ageGroup) {
        for(Result r : results) {
            if(r.getCompetitor().compareTo(competitor) == 0 &&
                    r.getDiscipline().compareTo(discipline) == 0 &&
                    r.getAgeGroup().compareTo(ageGroup) == 0
            ) {
                return r;
            }
        }

        return null;
    }
}
