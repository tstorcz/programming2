package Episode07b;

public class Competitor extends NamedComponent implements Comparable<Competitor> {
    private static String nameFormat = "l f";

    public static String getNameFormat() {
        return nameFormat;
    }

    public static void setNameFormat(String nameFormat) {
        Competitor.nameFormat = nameFormat;
    }

    private static String getName(String firstName, String lastName) {
        if(firstName.contains("(")) {
            firstName = firstName.substring(0, firstName.indexOf("(")).trim();
        }

        switch (nameFormat) {
            case "l f":
                return String.format("%s %s", lastName, firstName);
            case "l, f":
                return String.format("%s, %s", lastName, firstName);
            default:
                return String.format("%s %s", firstName, lastName);
        }
    }

    private int birthYear;
    private ComparableNamedComponent club;

    public int getBirthYear() {
        return birthYear;
    }

    public ComparableNamedComponent getClub() {
        return club;
    }

    public Competitor(int id, String name, int birthYear, ComparableNamedComponent club) {
        super(id, name);
        this.birthYear = birthYear;
        this.club = club;
    }

    public Competitor(int id, String firstName, String lastName, ComparableNamedComponent club) {
        super(id, Competitor.getName(firstName, lastName));
        this.birthYear = -1;
        this.club = club;

        if(firstName.contains("(")) {
            this.birthYear = Integer.parseInt(
                    firstName.substring(firstName.indexOf("(") + 1, firstName.indexOf(")"))
            );
            if(this.birthYear > 20)this.birthYear += 1900;
            else this.birthYear += 2000;
        }
    }

    public Competitor(Competitor source, ComparableNamedComponent club) {
        this(source.getId(), source.getName(), source.getBirthYear(), club);
    }

    @Override
    public int compareTo(Competitor o) {
        if(this.getId() >= 0 || o.getId() >= 0) {
            return Integer.compare(this.getId(), o.getId());
        }

        //Normal name comparison has no added information
        int nameComparison = this.getNameWithoutAccent().toLowerCase().compareTo(o.getNameWithoutAccent().toLowerCase());
        if(nameComparison != 0) {
            return nameComparison;
        }

        if(this.getBirthYear() >= 0 && o.getBirthYear() >= 0) {
            return Integer.compare(this.getBirthYear(), o.getBirthYear());
        }

        if(this.getClub().getId() >= 0 || o.getClub().getId() >= 0) {
            return Integer.compare(this.getClub().getId(), o.getClub().getId());
        }

        //Normal name comparison has no added information
        return this.getClub().getNameWithoutAccent().toLowerCase().compareTo(o.getClub().getNameWithoutAccent().toLowerCase());
    }

    @Override
    public String toString() {
        return super.toString() + "; " + birthYear + "; " + club.getName();
    }
}
