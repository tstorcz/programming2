package Episode07b;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BResultListLoader {
    public static BResultList load(String resultListName, String fileName) {
        File registrationFile = new File(fileName);
        BResultList resultList = new BResultList(resultListName);

        try {
            Scanner registrationReader = new Scanner(registrationFile);
            while (registrationReader.hasNextLine()) {
                String line = registrationReader.nextLine();
                try {
                    resultList.addResult(BResultParser.parse(line));
                }
                catch (NumberFormatException ex) {
                    System.out.printf("Could not parse result line: %s%n", line);
                }
            }
            registrationReader.close();
        } catch (FileNotFoundException e) {
            System.out.printf("Could not open result file: %s%n", fileName);
        }

        return resultList;
    }
}
