package Episode07b;

import java.util.ArrayList;

public class ResultDataSource extends DataSource {
    public ResultDataSource(ArrayList<BResultList> resultLists) {
        super();
        BResult r;
        ComparableNamedComponent club;
        Competitor competitor;
        ComparableNamedComponent ageGroup;
        ComparableNamedComponent discipline;
        for(BResultList rl : resultLists) {
            for(int i=0; i<rl.getSize(); i++) {
                r = rl.getResult(i);

                club = clubs.addItem(new ComparableNamedComponent(r.getClubName()));
                competitor = competitors.addItem(
                        new Competitor(
                                r.getCompetitorId(), r.getFirstName(), r.getLastName(), club
                        )
                );
                discipline = disciplines.addItem(new ComparableNamedComponent(rl.getName()));
                ageGroup = ageGroups.addItem(new ComparableNamedComponent(r.getAgeGroupName()));

                results.add(
                        new Result(competitor, discipline, ageGroup, r.getSeries(), r.getTotal(), r.isDecimalResult(), r.getRank())
                );
            }
        }
    }
}
