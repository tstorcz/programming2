package Episode07b;

public class ARegistration {
    private int competitorId;
    private String competitorName;
    private int competitorBirthYear;

    private int clubId;
    private String clubName;

    private int contestId;
    private int disciplineTypeId;
    private String disciplineTypeName;
    private int disciplineId;
    private String disciplineName;
    private int ageGroupId;
    private String ageGroupName;

    public int getContestId() {
        return contestId;
    }

    public int getDisciplineId() {
        return disciplineId;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public int getDisciplineTypeId() {
        return disciplineTypeId;
    }

    public String getDisciplineTypeName() {
        return disciplineTypeName;
    }

    public int getCompetitorId() {
        return competitorId;
    }

    public String getCompetitorName() {
        return competitorName;
    }

    public int getCompetitorBirthYear() {
        return competitorBirthYear;
    }

    public int getAgeGroupId() {
        return ageGroupId;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public int getClubId() {
        return clubId;
    }

    public String getClubName() {
        return clubName;
    }

    public ARegistration(int contestId, int disciplineId, String disciplineName, int disciplineTypeId, String disciplineTypeName, int competitorId, String competitorName, int competitorBirthYear, int ageGroupId, String ageGroupName, int clubId, String clubName) {
        this.contestId = contestId;
        this.disciplineId = disciplineId;
        this.disciplineName = disciplineName;
        this.disciplineTypeId = disciplineTypeId;
        this.disciplineTypeName = disciplineTypeName;
        this.competitorId = competitorId;
        this.competitorName = competitorName;
        this.competitorBirthYear = competitorBirthYear;
        this.ageGroupId = ageGroupId;
        this.ageGroupName = ageGroupName;
        this.clubId = clubId;
        this.clubName = clubName;
    }
}
