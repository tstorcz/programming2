package Episode07b;

public class AssociationList<T extends Comparable<T>> {
    private final Dictionary<Association<T>> associations = new Dictionary<>();

    public int getSize() { return associations.getSize(); }

    public Association<T> getItem(int index) { return associations.getItem(index); }

    public void addItem(Association<T> newItem) {
        associations.addItem(newItem);
    }

    public Association<T> findByA(T needle) {
        for (Association<T> a : associations) {
            if(a.getA().compareTo(needle) == 0)
                return a;
        }
        return null;
    }

    public Association<T> findByB(T needle) {
        for (Association<T> a : associations) {
            if(a.getB().compareTo(needle) == 0)
                return a;
        }
        return null;
    }
}
