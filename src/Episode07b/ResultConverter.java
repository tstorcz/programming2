package Episode07b;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class ResultConverter {
    //Registration file name should contain: "_registration_"
    public static final String DATASOURCE_A_FILE_PREFIX = "ep04b_a_";

    //All other files should be result data of type B
    public static final String DATASOURCE_B_FILE_PREFIX = "ep04b_b_";

    //Dictionary file names contain: "_dct_"
    public static final String CLUB_ASSOCIATION_FILE_NAME = "Ep07b_dct_clubs.csv";
    public static final String DISCIPLINE_ASSOCIATION_FILE_NAME = "Ep07b_dct_disciplines.csv";
    public static final String AGE_GROUP_ASSOCIATION_FILE_NAME = "Ep07b_dct_classes.csv";
    public static final String COMPETITOR_ASSOCIATION_FILE_NAME = "Ep07b_dct_competitors.csv";

    //Export file names contain: "_exp_"
    public static final String RESUL_LIST_FILE_NAME = "Ep07b_%d_exp_results.csv";
    public static final String NOT_PARTICIPATED_LIST_FILE_NAME = "Ep07b_%d_exp_not_participated.csv";
    public static final String NOT_REGISTERED_LIST_FILE_NAME = "Ep07b_%d_exp_not_registered.csv";
    public static final String MATCH_ERROR_LIST_FILE_NAME = "Ep07b_%d_exp_match_error.csv";

    private final ArrayList<ARegistration> registrations = new ArrayList<>();
    private final ArrayList<BResultList> results = new ArrayList<>();
    private Database database;

    public void convert(String dataPath) {
        loadDataSources(dataPath);

        createDatabaseWithCatalogs();

        loadAssociations(dataPath);
        printAssociationCounts();

        database.findDictionaryMatches();
        printAssociationCounts();

        database.copyResultsFromBToA();

        saveResultAList(Paths.get(dataPath, String.format(RESUL_LIST_FILE_NAME, database.getCompetitionId())).toString());
        saveNotParticipatedList(Paths.get(dataPath, String.format(NOT_PARTICIPATED_LIST_FILE_NAME, database.getCompetitionId())).toString());
        saveNotRegisteredList(Paths.get(dataPath, String.format(NOT_REGISTERED_LIST_FILE_NAME, database.getCompetitionId())).toString());
        saveMatchErrorList(Paths.get(dataPath, String.format(MATCH_ERROR_LIST_FILE_NAME, database.getCompetitionId())).toString());

        saveAssociations(dataPath);
    }

    private void printAssociationCounts() {
        System.out.println("Associations => Club: " + database.getClubAssociations().getSize() +
                ", Disciplines: " + database.getDisciplineAssociations().getSize() +
                ", Classes: " + database.getAgeGroupAssociations().getSize() +
                ", Competitors: " + database.getCompetitorAssociations().getSize()
        );
    }

    //1. Load data from different data sources
    private void loadDataSources(String dataPath) {
        File[] files = new File(dataPath).listFiles();
        for (File f : files) {
            if(f.isFile()
                    && f.getName().lastIndexOf(".") > 0
                    && f.getName().substring(f.getName().lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {

                if(f.getName().toLowerCase().startsWith(DATASOURCE_A_FILE_PREFIX)) {
                    loadRegistrations(f.getAbsolutePath());
                }
                if(f.getName().toLowerCase().startsWith(DATASOURCE_B_FILE_PREFIX)) {
                    results.add(
                            BResultListLoader.load(
                                    f.getName().substring(DATASOURCE_B_FILE_PREFIX.length(), f.getName().lastIndexOf(".")),
                                    f.getAbsolutePath()
                            )
                    );
                }
            }
        }
    }

    //1.a. Load registrations made in System A (RegistrationA)
    private void loadRegistrations(String fileName) {
        File registrationFile = new File(fileName);

        try {
            Scanner registrationReader = new Scanner(registrationFile);
            while (registrationReader.hasNextLine()) {
                String line = registrationReader.nextLine();
                try {
                    registrations.add(ARegistrationParser.parse(line));
                }
                catch (NumberFormatException ex) {
                    System.out.printf("Could not parse registration line: %s%n", line);
                }
            }
            registrationReader.close();
        } catch (FileNotFoundException e) {
            System.out.printf("Could not open registration file: %s%n", fileName);
        }
    }

    //1.b. Load results saved in System B (ResultB)
    //    Not a single file is loaded, but list of results
    //    Task is solved by BResultListLoader.load(...)

    //2. Create catalogs: club, competitor, discipline, class dictionaries and a registration/result list using their references
    private void createDatabaseWithCatalogs() {
        database = new Database(
                registrations.get(0).getContestId(),
                new RegistrationDataSource(registrations),
                new ResultDataSource(results)
        );
    }

    //3.a. Load previously saved matches if there are any (saved in #6)
    private void loadAssociations(String folderFullPath) {
        loadAssociation(
                Paths.get(folderFullPath, CLUB_ASSOCIATION_FILE_NAME).toString(),
                new AssociationParser<>(),
                database.getSourceA().getClubs(), database.getSourceB().getClubs(),
                s -> new ComparableNamedComponent(Integer.parseInt(s[0]), s[1]),
                database.getClubAssociations()
        );

        loadAssociation(
                Paths.get(folderFullPath, DISCIPLINE_ASSOCIATION_FILE_NAME).toString(),
                new AssociationParser<>(),
                database.getSourceA().getDisciplines(), database.getSourceB().getDisciplines(),
                s -> new ComparableNamedComponent(Integer.parseInt(s[0]), s[1]),
                database.getDisciplineAssociations()
        );

        loadAssociation(
                Paths.get(folderFullPath, AGE_GROUP_ASSOCIATION_FILE_NAME).toString(),
                new AssociationParser<>(),
                database.getSourceA().getAgeGroups(), database.getSourceB().getAgeGroups(),
                s -> new ComparableNamedComponent(Integer.parseInt(s[0]), s[1]),
                database.getAgeGroupAssociations()
        );

        loadAssociation(
                Paths.get(folderFullPath, COMPETITOR_ASSOCIATION_FILE_NAME).toString(),
                new AssociationParser<>(),
                database.getSourceA().getCompetitors(), database.getSourceB().getCompetitors(),
                s -> new Competitor(Integer.parseInt(s[0]), s[1], Integer.parseInt(s[2]), new ComparableNamedComponent(-1, s[3])),
                database.getCompetitorAssociations()
        );
    }

    private <T extends Comparable<T>> void loadAssociation(
            String fileName, AssociationParser<T> parser,
            Dictionary<T> a, Dictionary<T> b, Factory<T> factory,
            AssociationList<T> associationList
    ) {
        File registrationFile = new File(fileName);

        try {
            Scanner registrationReader = new Scanner(registrationFile);
            String line;
            Association<T> association;
            while (registrationReader.hasNextLine()) {
                line = registrationReader.nextLine();
                try {
                    association = parser.parseLine(line, a, b, factory);
                    if(association != null) {
                        associationList.addItem(association);
                    }
                }
                catch (NumberFormatException ex) {
                    System.out.printf("Could not parse association line: %s%n", line);
                }
            }
            registrationReader.close();
        } catch (FileNotFoundException e) {
            System.out.printf("Could not open association file: %s%n", fileName);
        }
    }

    //3.b. Find matches in catalogs automatically (by id or name)
    //    Task is solved by Database.findDictionaryMatches()

    //4. Assign registrations with results based on catalog matches
    //    Task is solved by Database.copyResultsFromBToA()

    //5. Create a result set compatible with System A (based on catalogs of A) -> ResultA
    private void saveResultAList(String fileName) {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);

            fw.write("VersenyId; VersenyszamId; VersenyszamNev; KorcsoportId; KorcsoportNev; VersenyzoId; VersenyzoNev; KlubId; KlubNev; Eredmeny; BelsoTizes; A; -; Sorozatok\n");

            for(Result r : database.getSourceA().getResults()) {
                if(r.getMatchCount() == 1) {
                    fw.write(database.getCompetitionId() + "; " + r + "A; -; " + r.getSeriesString() + "\n");
                }
            }

            fw.close();
        }
        catch (IOException e) {
            System.out.println("Could not write file: " + fileName);
        }
    }

    private void saveNotParticipatedList(String fileName) {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);

            fw.write("VersenyszamId; VersenyszamNev; KorcsoportId; KorcsoportNev; VersenyzoId; VersenyzoNev; KlubId; KlubNev\n");

            for(Result r : database.getSourceA().getResults()) {
                if(r.getMatchCount() == 0) {
                    fw.write(r.getStringWithoutResults() + "\n");
                }
            }

            fw.close();
        }
        catch (IOException e) {
            System.out.println("Could not write file: " + fileName);
        }
    }

    private void saveNotRegisteredList(String fileName) {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);

            fw.write("VersenyszamId; VersenyszamNev; KorcsoportId; KorcsoportNev; VersenyzoId; VersenyzoNev; KlubId; KlubNev; Eredmeny; Helyezes; Sorozatok\n");

            for(Result r : database.getSourceB().getResults()) {
                if(r.getMatchCount() == 0) {
                    fw.write(r.getStringWithoutResults() +
                            "; " + r.getTotal() +
                            "; " + r.getRank() +
                            "; " + r.getSeriesString());
                }
            }

            fw.close();
        }
        catch (IOException e) {
            System.out.println("Could not write file: " + fileName);
        }
    }

    private void saveMatchErrorList(String fileName) {
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);

            fw.write("Forras; VersenyszamId; VersenyszamNev; KorcsoportId; KorcsoportNev; VersenyzoId; VersenyzoNev; KlubId; KlubNev; EgyezesSzam; Eredmeny; Helyezes; Sorozatok\n");

            for(Result r : database.getSourceA().getResults()) {
                if(r.getMatchCount() > 1) {
                    fw.write("SVIR; " + r.getStringWithoutResults() +
                            "; " + r.getMatchCount() +
                            "; " + r.getTotal() +
                            "; " + r.getRank() +
                            "; " + r.getSeriesString());
                }
            }

            for(Result r : database.getSourceB().getResults()) {
                if(r.getMatchCount() > 1) {
                    fw.write("Meyton; " + r.getStringWithoutResults() +
                            "; " + r.getMatchCount() +
                            "; " + r.getTotal() +
                            "; " + r.getRank() +
                            "; " + r.getSeriesString());
                }
            }

            fw.close();
        }
        catch (IOException e) {
            System.out.println("Could not write file: " + fileName);
        }
    }

    //6. Store catalog matches to make next matching easier
    private void saveAssociations(String folderFullPath) {
        new AssociationParser<ComparableNamedComponent>().save(
                Paths.get(folderFullPath, CLUB_ASSOCIATION_FILE_NAME).toString(),
                database.getSourceA().getClubs(), database.getSourceB().getClubs(),
                database.getClubAssociations()
        );

        new AssociationParser<ComparableNamedComponent>().save(
                Paths.get(folderFullPath, DISCIPLINE_ASSOCIATION_FILE_NAME).toString(),
                database.getSourceA().getDisciplines(), database.getSourceB().getDisciplines(),
                database.getDisciplineAssociations()
        );

        new AssociationParser<ComparableNamedComponent>().save(
                Paths.get(folderFullPath, AGE_GROUP_ASSOCIATION_FILE_NAME).toString(),
                database.getSourceA().getAgeGroups(), database.getSourceB().getAgeGroups(),
                database.getAgeGroupAssociations()
        );

        new AssociationParser<Competitor>().save(
                Paths.get(folderFullPath, COMPETITOR_ASSOCIATION_FILE_NAME).toString(),
                database.getSourceA().getCompetitors(), database.getSourceB().getCompetitors(),
                database.getCompetitorAssociations()
        );
    }

}
