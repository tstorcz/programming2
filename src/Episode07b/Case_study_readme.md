# Case study
  
The task is to merge data from 2 different sources and create filtered subsets.
  
Data sources:
- registration system: contains registrations of competitors for disciplines  
- result system: contains results of competitors performed in disciplines
  
### Data:  
- registration file: ```Ep04b_A_nevezettek_6531.csv```
  - Competition id
  - Discipline id
  - Discipline name
  - Discipline type id
  - Discipline type name
  - Competitor id
  - Competitor name
  - Birth year
  - Age group id
  - Age group name
  - Club id
  - Club name
- result list file: ```Ep04b_B_<discipline_name>.csv```
  - Rank
  - StartNo
  - ShooterID
  - LastName
  - FirstName
  - Country
  - Discipline
  - Class
  - Club
  - Team
  - Series (2-6)
  - Total
  - Remarks
  
### Notes:
- a competitor could be registered for many disciplines
- many competitors could be registered for a discipline
- registration and result storage systems are separated and managed independently   
identifiers of registration and result storage systems are not synchronized
  (identifiers of same items are not surely match)
- names:
  - could be mistyped or could not contain accents of hungarian letters 
  - names could contain last 2 digits of birth year "Kiss Ivett (98)"
  - names are in hingarian format (Lastname Firstname)
- age group/class is not important for result list creation, but for start list creation
  
## Task 
Tasks to solve:
- match registrations with results automatically
- provide an option to set matches manually (save dictionary associations)
- create a result list based on registrations, containing all results of registered competitors who participated in the contest
  - ```Ep07b_<competitionId>_results.csv``` 
    - CompetitionId
    - DisciplineId
    - CompetitorId
    - TotalResult
    - InnerTens
    - "A" (constant)
    - "-" (constant)
  - ```Ep07b_<competitionId>_not_participated.csv```
  - ```Ep07b_<competitionId>_not_registered.csv```
  - ```Ep07b_<competitionId>_match_error.csv```
- list all competitors who were registered but not participated
- list all competitors who did participate but were not registered
- list all matching errors (registrations or results with ```match>1```)
- create a start list for result storage system (list of registered competitors based on dictionaries of storage system)
## Solution
Object-oriented solution containing encapsulation, inheritance and polymorphism.  
Also including abstract class, interface, generic solution with type parameter and lambda method.  
