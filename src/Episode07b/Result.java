package Episode07b;

public class Result {
    private final Competitor competitor;
    private final ComparableNamedComponent discipline;
    private final ComparableNamedComponent ageGroup;
    private double[] series;
    private double total;
    private boolean isDecimal;
    private int rank;
    private int matchCount;

    public Competitor getCompetitor() {
        return competitor;
    }

    public ComparableNamedComponent getDiscipline() {
        return discipline;
    }

    public ComparableNamedComponent getAgeGroup() {
        return ageGroup;
    }

    public double[] getSeries() {
        return series;
    }

    public double getTotal() {
        return total;
    }

    public boolean isDecimal() {
        return isDecimal;
    }

    public int getRank() {
        return rank;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public void resetMatchCount() {
        matchCount = 0;
    }

    public void addMatch() {
        matchCount++;
    }

    public Result(Competitor competitor, ComparableNamedComponent discipline, ComparableNamedComponent ageGroup) {
        this(competitor, discipline, ageGroup, null, 0, false, 0);
    }

    public Result(Competitor competitor, ComparableNamedComponent discipline, ComparableNamedComponent ageGroup, double[] series, double total, boolean isDecimal, int rank) {
        this.competitor = competitor;
        this.discipline = discipline;
        this.ageGroup = ageGroup;
        this.series = series;
        this.total = total;
        this.isDecimal = isDecimal;
        this.rank = rank;
    }

    public void copyResults(Result source) {
        series = source.getSeries();
        total = source.getTotal();
        isDecimal = source.isDecimal();
        rank = source.getRank();
    }

    public String getStringWithoutResults() {
        StringBuilder sb = new StringBuilder();

        sb.append(discipline.getId());sb.append("; ");
        sb.append(discipline.getName());sb.append("; ");
        sb.append(ageGroup.getId());sb.append("; ");
        sb.append(ageGroup.getName());sb.append("; ");

        sb.append(competitor.getId());sb.append("; ");
        sb.append(competitor.getName());sb.append("; ");
        sb.append(competitor.getClub());sb.append("; ");

        return sb.toString();
    }

    public String getSeriesString() {
        StringBuilder sb = new StringBuilder();
        if(series != null) {
            for (double s : series) {
                if(sb.length() > 0)sb.append("; ");
                sb.append(isDecimal ? String.format("%.1f", s) : String.format("%.0f", s));
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(discipline.getId());sb.append("; ");
        sb.append(discipline.getName());sb.append("; ");
        sb.append(ageGroup.getId());sb.append("; ");
        sb.append(ageGroup.getName());sb.append("; ");

        sb.append(competitor.getId());sb.append("; ");
        sb.append(competitor.getName());sb.append("; ");
        sb.append(competitor.getClub());sb.append("; ");

        sb.append(isDecimal ? String.format("%.1f", total) : String.format("%.0f", total));sb.append("; ");
        sb.append(-rank);sb.append("; ");

        return sb.toString();
    }
}
