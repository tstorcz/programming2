package Episode07b;

import java.util.Iterator;

public class DictionaryIterator<T extends Comparable<T>> implements Iterator<T> {
    private int nextIndex;
    Dictionary<T> datasource;

    public DictionaryIterator(Dictionary<T> obj)
    {
        nextIndex = 0;
        datasource = obj;
    }

    // Checks if the next element exists
    public boolean hasNext() {
        return nextIndex < datasource.getSize();
    }

    // moves the cursor/iterator to next element
    public T next() {
        return datasource.getItem(nextIndex++);
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

}
