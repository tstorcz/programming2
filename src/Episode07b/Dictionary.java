package Episode07b;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Dictionary<T extends Comparable<T>> implements Iterable<T> {
    private final ArrayList<T> items = new ArrayList<>();

    public int getSize() { return items.size(); }

    public T getItem(int index) { return items.get(index); }

    public T find(T needle) {
        for (T c : items) {
            if(c.compareTo(needle) == 0) return c;
        }

        return null;
    }

    public T addItem(T newItem) {
        T found = find(newItem);
        if(found == null) {
            items.add(newItem);
            return newItem;
        }
        else {
            return found;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return (Iterator<T>) new DictionaryIterator<>(this);
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return Iterable.super.spliterator();
    }
}
