package Episode07b;

public class BResult {
    private int competitorId;
    private String lastName;
    private String firstName;
    private String countryName;

    private String clubName;

    private int startNo;
    private String disciplineName;
    private String ageGroupName;
    private String team;

    private double[] series;
    private double total;
    private boolean decimalResult;
    private String remarks;

    private int rank;

    public int getRank() {
        return rank;
    }

    public int getStartNo() {
        return startNo;
    }

    public int getCompetitorId() {
        return competitorId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public String getClubName() {
        return clubName;
    }

    public String getTeam() {
        return team;
    }

    public double[] getSeries() {
        return series;
    }

    public double getTotal() {
        return total;
    }

    public boolean isDecimalResult() {
        return decimalResult;
    }

    public String getRemarks() {
        return remarks;
    }

    public BResult(int rank, int startNo, int competitorId, String lastName, String firstName, String countryName, String disciplineName, String ageGroupName, String clubName, String team, double[] series, double total, boolean decimalResult, String remarks) {
        this.rank = rank;
        this.startNo = startNo;
        this.competitorId = competitorId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.countryName = countryName;
        this.disciplineName = disciplineName;
        this.ageGroupName = ageGroupName;
        this.clubName = clubName;
        this.team = team;
        this.series = series;
        this.total = total;
        this.decimalResult = decimalResult;
        this.remarks = remarks;
    }
}
