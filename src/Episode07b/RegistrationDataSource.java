package Episode07b;

import java.util.ArrayList;

public class RegistrationDataSource extends DataSource {
    public RegistrationDataSource(ArrayList<ARegistration> registrations) {
        super();
        ComparableNamedComponent club;
        Competitor competitor;
        ComparableNamedComponent ageGroup;
        ComparableNamedComponent discipline;
        int charIndex;
        String disciplineName;
        String ageGroupName;
        for(ARegistration r : registrations) {
            club = clubs.addItem(new ComparableNamedComponent(r.getClubId(), r.getClubName()));
            competitor = competitors.addItem(
                    new Competitor(
                            r.getCompetitorId(), r.getCompetitorName(), r.getCompetitorBirthYear(), club
                    )
            );

            charIndex = r.getDisciplineName().lastIndexOf("-");
            ageGroupName = r.getDisciplineName().substring(charIndex + 1).trim();

            discipline = disciplines.addItem(
                    new ComparableNamedComponent(r.getDisciplineId(), r.getDisciplineName().trim())
            );
            ageGroup = ageGroups.addItem(new ComparableNamedComponent(ageGroupName));

            results.add(
                    new Result(competitor, discipline, ageGroup)
            );
        }
    }
}
