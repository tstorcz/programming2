package Episode07b;

public class ARegistrationParser {
    public static ARegistration parse(String line) {
        String[] values = line.replace("\"", "").split(";");

        return new ARegistration(
                Integer.parseInt(values[0]),
                Integer.parseInt(values[1]),
                values[2],
                Integer.parseInt(values[3]),
                values[4],
                Integer.parseInt(values[5]),
                values[6],
                Integer.parseInt(values[7]),
                Integer.parseInt(values[8]),
                values[9],
                Integer.parseInt(values[10]),
                values[11]
        );
    }
}
