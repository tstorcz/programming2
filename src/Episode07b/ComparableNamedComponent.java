package Episode07b;

public class ComparableNamedComponent extends NamedComponent implements Comparable<ComparableNamedComponent> {
    public ComparableNamedComponent(int id, String name) {
        super(id, name);
    }

    public ComparableNamedComponent(String name) {
        super(-1, name);
    }

    public int compareTo(ComparableNamedComponent o) {
        if(this.getId() >= 0 && o.getId() >= 0) {
            return Integer.compare(this.getId(), o.getId());
        }

        if(this.getName().toLowerCase().compareTo(o.getName().toLowerCase()) != 0)
            return this.getNameWithoutAccent().toLowerCase().compareTo(o.getNameWithoutAccent().toLowerCase());

        return 0;
    }
}
