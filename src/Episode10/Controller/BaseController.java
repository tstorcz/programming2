package Episode10.Controller;

import Episode09.System.SingletonNetworkLogger;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public abstract class BaseController {
    protected Scanner scanner;
    protected PrintStream outputStream;

    public BaseController(Scanner scanner, PrintStream outputStream) {
        this.scanner = scanner;
        this.outputStream = outputStream;
    }

    abstract public void start() throws IOException;

    protected int getNumber(
            String message,
            int min, int max,
            boolean acceptEmpty, int defaultValue
    ) throws IOException {
        String entered;
        boolean hasError = false;
        int number = defaultValue;
        do {
            if(this.outputStream != null)this.outputStream.printf("%s (%d-%d): ", message, min, max);
            if(acceptEmpty) {
                if(this.outputStream != null)this.outputStream.printf("[%s] ", defaultValue);
            }
            try {
                entered = scanner.nextLine();
                if(acceptEmpty && entered.isEmpty()){
                    return defaultValue;
                }
                number = Integer.parseInt(entered);
            } catch (NumberFormatException e) {
                hasError = true;
                if(this.outputStream != null)this.outputStream.printf("Please enter an integer (%d-%d):", min, max);
                SingletonNetworkLogger.getSharedInstance().write(
                        "Not entered an integer",
                        1
                );
            }
        }while (number < min || number > max || hasError);

        return number;
    }

    protected String getString(
            String message,
            boolean acceptEmpty, String defaultValue
    ) throws IOException {
        String entered;
        boolean hasError;
        do {
            hasError = false;
            if(this.outputStream != null) {
                this.outputStream.printf(
                        String.format(
                                "%s%s: ",
                                message,
                                (acceptEmpty)?String.format(" [%s]", defaultValue):""
                                )
                );
            }

            entered = scanner.nextLine();
            if(entered.isEmpty()){
                if(acceptEmpty) {
                    return defaultValue;
                }
                else {
                    hasError = true;
                    if(this.outputStream != null)this.outputStream.printf("%s (Not empty string): ", message);
                    SingletonNetworkLogger.getSharedInstance().write(
                            "Empty string entered, but refused.",
                            1
                    );
                }
            }

        }while(hasError);

        return entered;
    }
}
