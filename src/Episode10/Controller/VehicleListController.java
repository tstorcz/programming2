package Episode10.Controller;

import Episode10.Factory.BusFactory;
import Episode10.Factory.CarFactory;
import Episode10.Model.TooManyVehiclesException;
import Episode10.Model.VehicleStorage;
import Episode10.System.Localizer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class VehicleListController extends BaseController {
    private VehicleStorage site;

    public VehicleListController(Scanner consoleScanner, PrintStream outputStream, VehicleStorage site) {
        super(consoleScanner, outputStream);
        this.site = site;
    }

    @Override
    public void start() throws IOException {
        registerVehicles();

        //Change content to provide vehicle modification
    }

    private void registerVehicles() throws IOException {
        boolean repeat;
        VehicleController vehicleController;
        do {
            if(this.outputStream != null)
                this.outputStream.println(Localizer.getSharedInstance().translate("type_selection"));

            if(scanner.nextLine().compareToIgnoreCase(Localizer.getSharedInstance().translate("bus_char")
            ) == 0) {
                vehicleController = new VehicleController(scanner, outputStream, new BusFactory());
            }
            else {
                vehicleController = new VehicleController(scanner, outputStream, new CarFactory());
            }

            vehicleController.start();

            try {
                site.registerVehicle(vehicleController.getVehicle());
            } catch (IndexOutOfBoundsException e) {
                if(this.outputStream != null)
                    this.outputStream.println(Localizer.getSharedInstance().translate("too_many_vehicles"));
            }

            if(this.outputStream != null)
                this.outputStream.println(Localizer.getSharedInstance().translate("want_more"));

            repeat = scanner.nextLine().compareToIgnoreCase(Localizer.getSharedInstance().translate("yes_char")) == 0;
        }while(repeat);
    }
}
