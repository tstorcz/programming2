package Episode10.Controller;

import Episode10.Model.Vehicle;
import Episode10.Model.VehicleStorage;

import java.io.PrintStream;
import java.util.Scanner;

public class PrintVehicleListController extends BaseController {
    private VehicleStorage site;

    private String header;
    public String getHeader() {
        return header;
    }
    public void setHeader(String header) {
        this.header = header;
    }

    public PrintVehicleListController(Scanner consoleScanner, PrintStream outputStream, VehicleStorage site) {
        super(consoleScanner, outputStream);
        this.site = site;
    }

    @Override
    public void start() {
        printVehicleList();
    }

    private void printVehicleList() {
        if(this.outputStream != null) {
            this.outputStream.println(header);
            for (Vehicle v : site.getVehicles()) {
                this.outputStream.println(v);
            }
        }
    }
}
