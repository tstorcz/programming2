package Episode10.Controller;

import Episode10.System.Localizer;
import Episode10.Model.VehicleStorage;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class MainController extends BaseController {
    private VehicleStorage site = null;

    public MainController(Scanner scanner, PrintStream outputStream) {
        super(scanner, outputStream);
    }

    public void start() throws IOException {
        int selection;

        Localizer.getSharedInstance().setLanguageCode("en");

        do {
            createMenu();
            selection = getNumber(
                    Localizer.getSharedInstance().translate("select_item"),
                    0, 6, false, 0
            );
            executeOperation(selection);
        } while (selection != 6);
    }

    private void createMenu() {
        if(this.outputStream != null) {
            this.outputStream.println("0-" + Localizer.getSharedInstance().translate("selection"));
            if (site == null) {
                this.outputStream.println("1-" + Localizer.getSharedInstance().translate("registration_site"));
            } else {
                this.outputStream.println("2-" + Localizer.getSharedInstance().translate("registration_vehicle"));
                this.outputStream.println("3-" + Localizer.getSharedInstance().translate("list"));
                this.outputStream.println("4-" + Localizer.getSharedInstance().translate("sort"));
                this.outputStream.println("5-" + Localizer.getSharedInstance().translate("modify"));
            }
            this.outputStream.println("6-" + Localizer.getSharedInstance().translate("quit"));
        }
    }

    private void executeOperation(int operation) throws IOException {
        switch (operation) {
            case 0:
                new LanguageSelectorController(scanner, outputStream).start();
                break;
            case 1:
                SiteController siteController = new SiteController(scanner, outputStream, site);
                siteController.start();
                site = siteController.getSite();
                break;
            case 2:
                if(site != null) {
                    new VehicleListController(scanner, outputStream, site).start();
                }
                break;
            case 3:
                if(site != null) {
                    PrintVehicleListController printVehicleListController =
                            new PrintVehicleListController(scanner, outputStream, site);

                    printVehicleListController.setHeader(
                            Localizer.getSharedInstance().translate("list_header")
                    );
                    printVehicleListController.start();
                }
                break;
            case 4:
                if(site != null) {
                    site.sort();
                }
                break;
            case 5:
                if(site != null) {
                    //implement vehicle modification
                    //  1. select by index
                    //  2. modify content in VehicleController
                    //  ? is an update needed in the site (list of vehicles)
                }
                break;
        }
    }
}
