package Episode10.Controller;

import Episode10.Model.Vehicle;
import Episode10.Factory.VehicleFactory;
import Episode10.System.Localizer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class VehicleController extends BaseController {
    private Vehicle vehicle;
    private VehicleFactory factory;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public VehicleController(Scanner consoleScanner, PrintStream outputStream, VehicleFactory factory) {
        super(consoleScanner, outputStream);
        vehicle = null;
        this.factory = factory;
    }

    public VehicleController(Scanner consoleScanner, PrintStream outputStream, Vehicle vehicle) {
        super(consoleScanner, outputStream);
        this.vehicle = vehicle;
        this.factory = null;
    }

    @Override
    public void start() throws IOException {
        readVehicle();
    }

    private void readVehicle() throws IOException {
        String regNo = getString(
                Localizer.getSharedInstance().translate("reg_no"), false, ""
        );

        String makeModel = getString(
                Localizer.getSharedInstance().translate("make_model"), false, ""
        );

        int creationYear = getNumber(
                Localizer.getSharedInstance().translate("create_year"),
                1900, 2022, false, 2022
        );

        int numberOfSeats = getNumber(
                Localizer.getSharedInstance().translate("seat_count"),
                2, 200, false, 5
        );

        if(vehicle != null) {
            vehicle.setRegistrationNumber(regNo);
            vehicle.setMakeModel(makeModel);
            vehicle.setCreationYear(creationYear);
            vehicle.setNumberOfSeats(numberOfSeats);
        }
        else {
            vehicle = factory.createVehicle(regNo, makeModel, creationYear, numberOfSeats);
        }
    }
}
