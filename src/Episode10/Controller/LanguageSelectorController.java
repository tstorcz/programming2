package Episode10.Controller;

import Episode10.System.Localizer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class LanguageSelectorController extends BaseController {
    public LanguageSelectorController(Scanner consoleScanner, PrintStream outputStream) {
        super(consoleScanner, outputStream);
    }

    @Override
    public void start() throws IOException {
        boolean hasError;
        do {
            try {
                hasError = false;
                String newLangCode = getString(
                        Localizer.getSharedInstance().translate("language_code"),
                        false, ""
                );
                Localizer.getSharedInstance().setLanguageCode(newLangCode);
            }
            catch (IOException ex) {
                hasError = true;
            }
        }while(hasError);
    }
}
