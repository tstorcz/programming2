package Episode10.Controller;

import Episode10.Model.VehicleStorage;
import Episode10.System.Localizer;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class SiteController extends BaseController {
    private VehicleStorage site;

    public VehicleStorage getSite() {
        return site;
    }

    public SiteController(Scanner consoleScanner, PrintStream outputStream, VehicleStorage site) {
        super(consoleScanner, outputStream);
        this.site = site;
    }

    public void start() throws IOException {
        int placeCount = getNumber(
                Localizer.getSharedInstance().translate("place_count"),
                1, 100, false, 0
        );
        site = new VehicleStorage(placeCount);
    }

}
