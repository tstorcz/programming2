package Episode10.System;

import java.util.Date;

public class LogMessage {
    private String message;
    private int logLevel;
    private Date createDate;

    public String getMessage() {
        return message;
    }

    public int getLogLevel() {
        return logLevel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public LogMessage(String message, int logLevel, Date createDate) {
        this.message = message;
        this.logLevel = logLevel;
        this.createDate = createDate;
    }
}
