package Episode10.System;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

public class Localizer {
    private static Localizer sharedInstance = null;

    public static Localizer getSharedInstance() {
        if(sharedInstance == null) {
            sharedInstance = new Localizer();
        }
        return sharedInstance;
    }

    private Hashtable<String, String > dictionary = new Hashtable<>();

    private String languageCode;

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) throws IOException {
        Hashtable<String, String > newDictionary = new Hashtable<>();

        FileReader reader = new FileReader("Data\\Ep10\\Ep10_dictionary_" + languageCode + ".txt");
        BufferedReader br = new BufferedReader(reader);
        String line;
        while ((line = br.readLine()) != null) {
            String[] expressions = line.split("-");
            newDictionary.put(expressions[0], expressions[1]);
        }
        reader.close();

        dictionary = newDictionary;
        this.languageCode = languageCode;
    }

    public String translate(String defaultMessage) {
        if(!dictionary.containsKey(defaultMessage))
            return defaultMessage;

        return dictionary.get(defaultMessage);
    }
}
