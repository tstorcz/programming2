package Episode10.System;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Log {

    private static Log sharedInstance = null;

    public static Log getSharedInstance() {
        if(sharedInstance == null) {
            sharedInstance = new Log();
        }
        return sharedInstance;
    }

    private String fileName;
    private int logLogLevel;
    private boolean isOnline;
    private ArrayList<LogMessage> logBuffer;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getLogLogLevel() {
        return logLogLevel;
    }

    public void setLogLogLevel(int logLogLevel) {
        this.logLogLevel = logLogLevel;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) throws IOException {
        isOnline = online;
        if(isOnline){
            for(LogMessage m: logBuffer) {
                sendLogMessage(m);
            }
            logBuffer.clear();
        }
    }

    private Log() {
        fileName = "Data\\Events.log";
        logLogLevel = 5;
        logBuffer = new ArrayList<>();
    }

    public void write(String message, int messageLogLevel) throws IOException {
        if(messageLogLevel <= logLogLevel) {
            LogMessage newMessage = new LogMessage(
                    message,  messageLogLevel, new Date()
            );

            if(isOnline) {
                sendLogMessage(newMessage);
            }
            else {
                logBuffer.add(newMessage);
            }
        }
    }

    private void sendLogMessage(LogMessage message) throws IOException {
        DateFormat formatter = new SimpleDateFormat(
                "yyyy.MM.dd hh:mm:ss"
        );
        FileWriter writer = new FileWriter(fileName, true);
        writer.write(
                String.format("[%s] %s",
                        formatter.format(message.getCreateDate()),
                        message.getMessage()
                )
        );
        writer.close();
    }
}
