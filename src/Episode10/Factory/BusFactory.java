package Episode10.Factory;

import Episode10.Model.Bus;
import Episode10.Model.Vehicle;

public class BusFactory extends VehicleFactory {
    @Override
    public Vehicle createVehicle(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        return new Bus(registrationNumber, makeModel, creationYear, numberOfSeats);
    }
}
