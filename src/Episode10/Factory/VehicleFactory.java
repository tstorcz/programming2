package Episode10.Factory;

import Episode10.Model.Vehicle;

public abstract class VehicleFactory {

    abstract public Vehicle createVehicle(
            String registrationNumber,
            String makeModel,
            int creationYear,
            int numberOfSeats
    );

}
