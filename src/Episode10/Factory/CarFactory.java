package Episode10.Factory;

import Episode10.Model.Car;
import Episode10.Model.Vehicle;

public class CarFactory extends VehicleFactory {
    @Override
    public Vehicle createVehicle(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        return new Car(registrationNumber, makeModel, creationYear, numberOfSeats);
    }
}
