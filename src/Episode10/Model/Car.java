package Episode10.Model;

public class Car extends Vehicle {
    public Car(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        super(registrationNumber, makeModel, creationYear, numberOfSeats);
    }

}
