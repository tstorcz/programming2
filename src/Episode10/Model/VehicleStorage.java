package Episode10.Model;

import java.util.ArrayList;
import java.util.Collections;

public class VehicleStorage {
    private ArrayList<Vehicle> vehicles;
    private int maxItemCount;

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    public VehicleStorage(int maxItemCount) {
        vehicles = new ArrayList<>();
        this.maxItemCount = maxItemCount;
    }

    public void registerVehicle(Vehicle newVehicle) {
        if(vehicles.size() < maxItemCount) {
            vehicles.add(newVehicle);
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void sort() {
        Collections.sort(vehicles);
    }
}
