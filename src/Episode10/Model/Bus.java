package Episode10.Model;

public class Bus extends Vehicle {
    public Bus(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        super(registrationNumber, makeModel, creationYear, numberOfSeats);
    }
}
