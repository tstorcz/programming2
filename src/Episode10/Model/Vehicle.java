package Episode10.Model;

public abstract class Vehicle implements Comparable<Vehicle> {
    private String registrationNumber;
    private String makeModel;
    private int creationYear;
    private int numberOfSeats;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public int getCreationYear() {
        return creationYear;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public void setCreationYear(int creationYear) {
        this.creationYear = creationYear;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Vehicle(String registrationNumber, String makeModel, int creationYear, int numberOfSeats) {
        this.registrationNumber = registrationNumber;
        this.makeModel = makeModel;
        this.creationYear = creationYear;
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s (%d)",
                registrationNumber,
                makeModel,
                creationYear);
    }

    @Override
    public int compareTo(Vehicle o) {
        return registrationNumber.compareTo(o.getRegistrationNumber());
    }
}
