package Episode10;

import Episode10.System.Localizer;
import Episode10.System.Log;
import Episode10.Controller.MainController;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public void main() throws IOException {

        try {
            Log.getSharedInstance().write(
                    "Application started",
                    3
            );

            new MainController(
                    new Scanner(System.in), System.out
            ).start();

        }catch(IOException e) {
            Log.getSharedInstance().write(
                    Localizer.getSharedInstance().translate("") + ": " + e.getMessage(),
                    1
            );
        }
    }
}
