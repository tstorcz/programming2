package Episode07;

public class Bus extends PassengerTransporter {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Bus.baseRentalFee = baseRentalFee;
    }

    public Bus(String licence, int prodYear, double consumption, int passengers) {
        super(licence, prodYear, consumption, passengers);
    }

    public String toString() {
        return String.format("Bus %s passengers: %d",
                super.toString(), getNumberOfPassengers()
        );
    }
}
