package Episode07;

//Class is inherited from Vehicle, but also conforms explicit interface
//That replaces multiple inheritance
//Abstract class does not represent real world object
//therefore can not be instantiated
public abstract class PassengerTransporter
        extends Vehicle
        implements IPassengerTransporter
{
    //Data member is not prescribed by interface (internal state)
    private final int numberOfPassengers;

    //Overriding prescribed behavior is not mandatory here - Try to delete!
    @Override
    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public PassengerTransporter(
            String licence, int prodYear, double consumption,
            int passengers
    ) {
        super(licence, prodYear, consumption);
        this.numberOfPassengers = passengers;
    }
}
