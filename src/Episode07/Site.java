package Episode07;

import java.util.ArrayList;

public class Site {
    private final String name;
    private final String address;
    private final String id;
    private final int maxVehicleCount;
    private final IPrinter outChannel;

    //========================== Site related methods
    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public Site(String name, String address, String id, int maxVehicleCount, IPrinter outChannel) {
        this.name = name;
        this.address = address;
        this.id = id;
        this.maxVehicleCount = maxVehicleCount;
        this.vehicles = new ArrayList<>();
        this.outChannel = outChannel;
    }

    //========================== Content (item) related methods
    //Base class is used as type parameter of a generic class
    private final ArrayList<Vehicle> vehicles;

    public int getMaxVehicleCount() {
        return maxVehicleCount;
    }

    public int getVehicleCount() {
        return vehicles.size();
    }

    public Vehicle getVehicle(int index) {
        return vehicles.get(index);
    }

    public void registerVehicle(Vehicle newVehicle) {
        if(vehicles.size() < maxVehicleCount) {
            vehicles.add(newVehicle);
        }
    }

    //========================== Search methods
    public Vehicle find(String licence) {
        for(Vehicle v: vehicles) {
            if(v.getLicence().equalsIgnoreCase(licence)) {
                return v;
            }
        }
        return null;
    }

    public ArrayList<Bus> findBus(int numberOfPersons) {
        ArrayList<Bus> found = new ArrayList<>();
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Bus &&
                    ((Bus)v).getNumberOfPassengers() >= numberOfPersons
            ) {
                found.add((Bus)v);
            }
        }
        return found;
    }

    public ArrayList<Car> findCar(int numberOfPersons, int comfortLevel) {
        ArrayList<Car> found = new ArrayList<>();
        for(Vehicle v: vehicles) {
            if(
                    v instanceof Car
                    && ((Car)v).getNumberOfPassengers() >= numberOfPersons
                    && ((Car)v).getComfortLevel() >= comfortLevel
            ) {
                found.add((Car)v);
            }
        }
        return found;
    }

    public ArrayList<PassengerTransporter> findPassengerTransporterVehicle(int numberOfPersons) {
        ArrayList<PassengerTransporter> found = new ArrayList<>();
        for(Vehicle v: vehicles) {
            if(
                    v instanceof PassengerTransporter
                            && ((PassengerTransporter)v).getNumberOfPassengers() >= numberOfPersons
            ) {
                found.add((PassengerTransporter)v);
            }
        }
        return found;
    }

    public ArrayList<ICargo> findPassengerTransporter(int maxWeight) {
        ArrayList<ICargo> found = new ArrayList<>();
        for(Vehicle v: vehicles) {
            if(
                    v instanceof ICargo
                            && ((ICargo)v).getMaxWeight() >= maxWeight
            ) {
                found.add((ICargo)v);
            }
        }
        return found;
    }

    //=========================== Generic filter

    public <T> ArrayList<T> findItems(IFilter<T> filter) {
        ArrayList<T> found = new ArrayList<>();
        for(Vehicle v: vehicles) {
            if(filter.isSelected((T)v)) {
                found.add((T)v);
            }
        }
        return found;
    }

    public ArrayList<Bus> findBus2(int numberOfPersons){
        return findItems(
                v -> v instanceof Bus && ((Bus)v).getNumberOfPassengers() >= numberOfPersons
        );
    }

    //=========================== Printers
    //Should not be here, console management is out of responsibility of this class
    public void printListToConsole(ArrayList<Vehicle> listToPrint) {
        for (Vehicle v : listToPrint) {
            System.out.println(v.toString());
        }
    }

    //injected output channel
    public void printList(ArrayList<Vehicle> listToPrint) {
        for (Vehicle v : listToPrint) {
            outChannel.print(v.toString());
        }
    }

    public void printVehicles(){
        printList(vehicles);
    }

    public void printBuses(int numberOfPersons){
        printList(
                findItems(
                        v -> v instanceof Bus && ((Bus)v).getNumberOfPassengers() >= numberOfPersons
                )
        );
    }

    public void printCars(int numberOfPersons, int comfortLevel){
        printList(
                findItems(
                        v ->
                            v instanceof Car
                                    && ((Car)v).getNumberOfPassengers() >= numberOfPersons
                                    && ((Car)v).getComfortLevel() >= comfortLevel
                )
        );
    }

    public void printPassengerTransporters(int numberOfPersons){
        printList(
                findItems(
                        v ->
                            v instanceof PassengerTransporter
                                    && ((PassengerTransporter)v).getNumberOfPassengers() >= numberOfPersons
                )
        );
    }
}






