package Episode07;

public interface IFilter<T> {
    boolean isSelected(T object);
}
