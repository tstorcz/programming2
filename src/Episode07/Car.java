package Episode07;

public class Car extends PassengerTransporter {
    private static int baseRentalFee;

    public static int getBaseRentalFee() {
        return baseRentalFee;
    }

    public static void setBaseRentalFee(int baseRentalFee) {
        Car.baseRentalFee = baseRentalFee;
    }

    private int comfortLevel;

    public int getComfortLevel() {
        return comfortLevel;
    }

    public Car(
            String licence, int prodYear, double consumption, int passengers,
            int comfortLevel
    ) {
        super(licence, prodYear, consumption, passengers);
        this.comfortLevel = comfortLevel;
    }

    @Override
    public String toString() {
        return String.format("Car %s [passengers: %d] comfort level: %d",
                super.toString(), getNumberOfPassengers(), comfortLevel
        );
    }
}
