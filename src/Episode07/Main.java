package Episode07;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public void main() {
        Scanner consoleScanner = new Scanner(System.in);

        // Get site parameters
        System.out.print("Site name: ");
        String name = consoleScanner.nextLine();

        System.out.print("Site address: ");
        String address = consoleScanner.nextLine();

        System.out.print("Site identifier: ");
        String siteId = consoleScanner.nextLine();

        System.out.print("Max. vehicle count: ");
        int maxVehicles = consoleScanner.nextInt();

        //Create site
        Site mySite = new Site(
                name, address, siteId, maxVehicles,
                new ConsolePrinter()
                //new FilePrinter("messages.txt")
        );

        //Register vehicles of site
        mySite.registerVehicle(
                new Car("ABC123", 2002, 7.0, 5, 3)
        );

        mySite.registerVehicle(
                new Bus("ABC124", 2012, 15.0, 25)
        );

        mySite.registerVehicle(
                new Car("ABC125", 2009, 5.0, 5, 4)
        );

        mySite.registerVehicle(
                new Bus("ABC126", 2016, 9.7, 19)
        );

        mySite.registerVehicle(
                new Truck("ABC225", 2014, 18.7, 2.6)
        );

        mySite.registerVehicle(
                new Car("ABC325", 2019, 9.0, 2, 5)
        );

        //List vehicles of site
        listVehicles(mySite);

        System.out.println("Find specific vehicles...");

        System.out.println(mySite.findBus(10));
        System.out.println(mySite.findCar(2, 4));

        System.out.println(mySite.findPassengerTransporterVehicle(5));
        System.out.println(mySite.findPassengerTransporter(5));





        //======================== Using explicit interface as type =====================================
        System.out.println("============================================");
        System.out.println("============================================");

        Elevator elevator1 = new Elevator(12, 1000);

        //Explicit interface is used as type parameter of generic class
        ArrayList<IPassengerTransporter> passengerTransporters = new ArrayList<>();

        //Car and Bus conform IPassengerTransporter interface
        //Therefore Car and Bus can be implicitly casted to IPassengerTransporter
        passengerTransporters.add((Car)mySite.getVehicle(0));
        passengerTransporters.add((Bus)mySite.getVehicle(1));

        //Explicit cast could be done directly to IPassengerTransporter interface
        passengerTransporters.add((Car)mySite.getVehicle(2));
        passengerTransporters.add((Bus)mySite.getVehicle(3));

        //passengerTransporters.add((Truck)mySite.getVehicle(4));
        //Not valid: Truck does not conform IPassengerTransporter interface

        passengerTransporters.add(elevator1);

        listPassengerTransporters(passengerTransporters);

        //Collection could be enumerated, watch the type of iterator variable
        System.out.println("Find specific passenger transporters...");
        for (IPassengerTransporter pt : passengerTransporters) {
            if(pt.getNumberOfPassengers() >= 10) {
                //Iterator can be printed, because this is a reference,
                //reference points to an object,
                //object has toString method (inherited from Object class)
                System.out.println(pt);
            }
        }

        //=========================== Printers
        System.out.println("============================================");
        System.out.println("============================================");
        System.out.println("Using vehicle printers...");
        mySite.printVehicles();
        System.out.println("---");
        mySite.printBuses(10);
        System.out.println("---");
        mySite.printCars(2, 4);
        System.out.println("---");
        mySite.printPassengerTransporters(5);
    }

    private void listVehicles(Site site) {
        System.out.printf("========== Vehicles of site: %s%n", site.getName());
        for(int i = 0; i < site.getVehicleCount(); i++) {
            System.out.println(site.getVehicle(i));
        }
        System.out.println("----------");
    }

    //Explicit interface is used as type parameter of generic class in formal parameter list
    private void listPassengerTransporters(ArrayList<IPassengerTransporter> passengerTransporters) {
        System.out.println("========== List of passenger transporters");

        //Collection could be enumerated, watch the type of iterator variable
        for (IPassengerTransporter pt : passengerTransporters) {
            //Iterator can be printed, because this is a reference,
            //reference points to an object,
            //object has toString method (inherited from Object class)
            System.out.println(pt);
        }

        System.out.println("----------");
    }
}
