package Episode07;

public interface IPassengerTransporter {
    //Only a behavior is required
    //If it looks like a getter, internal state descriptor is not part of interface
    int getNumberOfPassengers();
}
