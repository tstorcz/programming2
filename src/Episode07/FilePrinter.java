package Episode07;

public class FilePrinter implements IPrinter {

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public FilePrinter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void print(String line) {
        //Print message line into a file specified by fileName
    }
}
