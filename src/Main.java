import Episode11.*;

import java.io.IOException;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        //In Edit Configurations -> Run/Debug configurations, set Program arguments string
        //that string will be split into array of strings
        String currentEpisode = "01";
        String dataPath = "Data";

        if(args.length > 0) {
            currentEpisode = args[0];
        }
        if(args.length > 1) {
            dataPath = args[1];
        }

        switch(currentEpisode) {
            case "01":
                Episode01.BasicAlgorithms.Sum();
                Episode01.BasicAlgorithms.Extrema();
                Episode01.BasicAlgorithms.Selection();
                Episode01.BasicAlgorithms.Sort();

                Episode01.BasicAlgorithms.SumManualInput();

                //In MVC (Model-View-Controller) design pattern:
                //Controller creates connection between user input/output (View) and data storage (Model)
                Episode01.Controller.main();
                break;
            case "02":
                //Example project - encapsulation: Classes, Objects
                Episode02.Controller.main();
                break;
            case "02b":
                //Practice task - homework
                new Episode02b.QuestionBankController().main();
                break;
            case "03":
                //Example project - relation of object: managing object references
                new Episode03.Main().main(5, dataPath);
                break;
            case "04":
                Episode04.Main mainEp4 = new Episode04.Main();

                //Example of polymorphism introduction
                mainEp4.polymorphismTest();

                //Example project - inheritance, polymorphism, cast
                mainEp4.main();
                break;
            case "04b":
                //Practice task - homework
                new Episode04b.ResultConverter().main(dataPath);
                break;
            case "05":
                new Episode05.Main().main();
                break;
            case "07":
                new Episode07.Main().main();
                break;
            case "07b":
                //Case study
                new Episode07b.ResultConverter().convert(Paths.get(dataPath, "Ep04").toString());
                break;
            case "08":
                // Episode 8 requires a dependency to be downloaded
                //      name: gson-2.8.2.jar (newer version could also work)
                //      source: https://github.com/google/gson
                //          Look for: Gson jar downloads are available from Maven Central.
                //          Select version, then download
                //      setup: https://www.jetbrains.com/help/idea/working-with-module-dependencies.html#add-a-new-dependency
                //              copy downloaded jar file to Episode08 folder
                new Episode08.Main().main();
                break;
            case "08b":
                new Episode08b.Main().start();
                break;
            case "09":
                new Episode09.Main().main();
                break;
            case "10":
                new Episode10.Main().main();
                break;
            case "11":
                WeightedAverageEvaluator wae = new WeightedAverageEvaluator(new double[] {1.0, 1.0, 3.0});
                CourseDescriptor courseDescr = new CourseDescriptor(
                        new ExamTypeDescriptor[] {
                                new ExamTypeDescriptor("Elmélet ", "Elmélet {exam} {try}.csv", wae),
                                new ExamTypeDescriptor("Gyakorlat ", "Gyakorlat {exam} {try}.csv", wae)
                        },
                        3, 1,
                        new WeightedAverageEvaluator(new double[] {1.0, 1.0}),
                        Paths.get(dataPath, "Ep11").toString()
                );

                new Episode11.Controller().process(courseDescr);
                break;
            case "12":
                new Episode12.Main().start();
                break;
        }

    }
}
