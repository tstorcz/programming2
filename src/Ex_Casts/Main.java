package Ex_Casts;

public class Main {
    public void main() {
        ClassE e = new ClassE();

        //Implicit upcasts
        ClassB b = e;
        ClassA a = e;
        ClassA a2 = b;

        //explicit downcast to source object are valid
        ClassE e2 = (ClassE)b;
        ClassE e3 = (ClassE)a;
        ClassE e4 = (ClassE)a2;

        //Downcast to descendant
        //Explicit cast valid at compile time
        //but will fail at run time: ClassCastException
        //ClassF f1 = e;
        ClassF f2 = (ClassF)e;
        ClassF f3 = (ClassF)b;
        ClassF f4 = (ClassF)a;
        ClassF f5 = (ClassF)a2;

        //Casts to different branch
        //Explicit cast valid at compile time
        //but will fail at run time_ ClassCastException
        //ClassD d1 = e;
        //ClassD d2 = (ClassD)e;
        ClassD d3 = (ClassD)b;
        ClassD d4 = (ClassD)a;
        ClassD d5 = (ClassD)a2;
    }

    class Fruit {
        void change() { }
    }

    class Apple extends Fruit{
    }

    private <E extends Fruit> E change(E e) {
        e.change();
        return e;
    }

    public void test() {
        boolean b1 = change(new Apple()) instanceof Fruit;
        boolean b2 = change(new Apple()) instanceof Apple;
        boolean b3 = change(new Fruit()) instanceof Apple;
        System.out.println(b1 + "/" + b2 + "/" + b3);
    }

}
