# Practice contents of Object-Oriented Programming in Java course #
for Computer Science Engineering BsC

Having basic C knowledge is a requirement about:
1. variables
2. arrays
3. control statements (sequence, selection, iteration)
4. methods, formal and actual parameters
5. pointers (address of, value of operators)
6. dynamic memory management
  
Contains a case study detailed and solved in Episode07b    
Details: [Case study readme](src/Episode07b/Case_study_readme.md)
  
## Prerequisites
For getting the project work, gson.jar file must be downloaded.
  
Requires a dependency to be downloaded
- name: gson-2.8.2.jar (newer version could also work)
- source: https://github.com/google/gson  
         Look for: Gson .JAR downloads are available from Maven Central.  
         Select version, then download  
- setup: https://www.jetbrains.com/help/idea/working-with-module-dependencies.html#add-a-new-dependency  
         copy downloaded .JAR file to Episode08 folder

University of Pecs  
Faculty of Engineering and Information Technology